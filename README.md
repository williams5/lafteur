# Template Android Project

This is a full Template Android Project with all library need
<div align="center">
  <img src="https://i.ibb.co/cF2Tsfs/70edb9362606ea314032b2bd610bd347.png">
</div>
<div align="center">
 <font size="20"><b>Dagger, Reactivex, Android Architecture Component</b></font>
</div>
 

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
 Android Studio Updated min 3.3
 Gradle 3.3 

### Installing



## All library dependency 

- blabla

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Jitpack](https://jitpack.io/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Appndigital** - *Initial work* - [AppNdigital](https://www.appndigital.com/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details



