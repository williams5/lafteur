/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.constants

class Constants {

    companion object {
        const val USER_PREFERENCES_KEY = "USER_PREFERENCES_KEY"
        const val USER_TOKEN_PREF_KEY = "USER_TOKEN_PREF_KEY"
        const val USER_ID_PREF_KEY = "USER_ID_PREF_KEY"
        const val USER_REFRESH_TOKEN_PREF_KEY = "USER_REFRESH_TOKEN_PREF_KEY"
        const val DEV_URL = "https://dev.habana.appndesk.com/"
        const val PROD_URL = "https://habana.appndesk.com/"

        const val BUNDLE_URI_PICTURE_CREATE_EVENT = "BUNDLE_URI_PICTURE_CREATE_EVENT"
        const val BUNDLE_ID_EVENT = "BUNDLE_ID_EVENT"
        const val BUNDLE_DESCRIPTION_EVENT = "BUNDLE_DESCRIPTION_EVENT"
        const val BUNDLE_EVENT_MODEL= "BUNDLE_EVENT_MODEL"

        const val LOGIN_BY_FB = "facebook"
        const val LOGIN_BY_PHONE = "phone"

        const val TIMEOUT_NUMBER_VERIF: Long = 60
    }
}