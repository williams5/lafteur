/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.constants


class Enums {


    enum class Gender constructor(val value: String) {
        MALE("male"),
        FEMALE("female");

        companion object {
            fun from(findValue: String): Gender = Gender.values().first { it.value == findValue }
        }
    }


    enum class HTTPStatus constructor(val code: Int) {
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        FORBIDDEN(403),
        BAD_EMAIL(422),
        NOT_FOUND(404),
        INTERNAL_SERVER_ERROR(500);

        companion object {
            fun from(findValue: Int): HTTPStatus = HTTPStatus.values().first { it.code == findValue }
        }
    }

    enum class SatusLogin {
        FB_LOGIN_SUCCESS,
        PHONE_LOGIN_SUCCESS,
        NOT_REGISTERED,
        LOGIN_FAILED
    }

    enum class EventType {
        REQUEST_EVENT,
        ALL_EVENT
    }

    enum class OnVerificationStateChanged {
        ON_COMPLETED,
        ON_FAILED,
        ON_QUOTA_EXCEED,
        ON_CODE_SENT
    }

    enum class StateUserRequest constructor(val value: String) {
        ACCEPTED("accepted"),
        DENIED("denied"),
        WAITING("waiting");

        companion object {
            fun from(findValue: String): StateUserRequest = values().first { it.value == findValue }
        }
    }

    /* Different exemple
    enum class HabanaGroupColor {
        BLUE,
        PERIWINKLE,
        ORANGE,
        PURPLE,
        NONE
    }


    enum class PaymentMode {
        @Json(name = "CASH")
        CASH,
        @Json(name = "PAYPAL")
        PAYPAL
    }
    */
}

