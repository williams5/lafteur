package com.lafteur.app.helpers.firestore

interface FirestorePagedDataSource {

    fun loadInitial()
    fun loadNext()
    fun clear()

}