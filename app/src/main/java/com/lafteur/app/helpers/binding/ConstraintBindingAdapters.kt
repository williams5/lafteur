package com.lafteur.app.helpers.binding

import androidx.constraintlayout.widget.Guideline
import androidx.databinding.BindingAdapter

@BindingAdapter("app:layout_constraintGuide_end")
fun Guideline.changeEnd(margin: Float) = setGuidelineEnd(margin.toInt())