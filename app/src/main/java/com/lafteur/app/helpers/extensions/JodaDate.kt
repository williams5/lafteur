package com.lafteur.app.helpers.extensions

import com.google.firebase.Timestamp
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.Locale

fun DateTime.toHumanDate() : String{
    return this.toString("d MMMM HH:mm") ?: ""
}

fun Timestamp.toHumanDate(): String {
    return getSimpleDateFormat("d MMMM HH:mm").format(this.toDate())
}


 private fun getSimpleDateFormat(format:String): SimpleDateFormat {
    return SimpleDateFormat(format, Locale.FRANCE)
}
