package com.lafteur.app.helpers.livedata

import android.os.Looper
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

inline fun <T> LiveData<T>.observeNotNull(lifecycleOwner: LifecycleOwner, crossinline observer: (T) -> Unit) {
    observe(lifecycleOwner, Observer { value ->
        if (value != null) observer(value)
    })
}

inline fun <T> LiveData<T>.observeChanges(
    lifecycleOwner: LifecycleOwner,
    crossinline observer: (oldValue: T?, newValue: T?) -> Unit
) {
    observe(lifecycleOwner, object : Observer<T> {
        private val changed = AtomicBoolean()

        private var oldValue: T? = null

        override fun onChanged(t: T) {
            if (changed.compareAndSet(false, true)) {
                observer(null, t)
            } else {
                observer(oldValue, t)
            }
            oldValue = t
        }
    })
}

fun <T> MutableLiveData<T>.changeValue(value: T?) = if (Looper.getMainLooper() == Looper.myLooper()) {
    setValue(value)
} else {
    postValue(value)
}

fun <T> MutableLiveData<T>.refresh() = changeValue(value)

fun MutableLiveData<Boolean>.toggle() = changeValue(!(value ?: false))

object EmptyObserver : Observer<Any?> {
    override fun onChanged(t: Any?) {
        // Silent
    }
}

@Suppress("UNCHECKED_CAST")
fun <T> emptyObserver() = EmptyObserver as Observer<T>