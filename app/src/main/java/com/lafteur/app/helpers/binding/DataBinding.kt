package com.lafteur.app.helpers.binding

import android.view.View
import androidx.annotation.IdRes
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.databinding.ObservableMap
import androidx.databinding.adapters.ListenerUtil

abstract class ReadOnlyObservableBoolean(vararg dependencies: Observable) : ObservableBoolean(*dependencies) {
    override fun set(value: Boolean) = throw UnsupportedOperationException()
}

abstract class ReadOnlyObservableField<T>(vararg dependencies: Observable) : ObservableField<T>(*dependencies) {
    override fun set(value: T) = throw UnsupportedOperationException()
}

abstract class ReadOnlyObservableInt(vararg dependencies: Observable) : ObservableInt(*dependencies) {
    override fun set(value: Int) = throw UnsupportedOperationException()
}

fun ObservableBoolean.toggle() = set(!get())

inline fun <V : View, L, AR, RR> V.addAndTrack(
    listener: L?,
    @IdRes idRes: Int,
    addAction: V.(L) -> AR,
    removeAction: V.(L) -> RR
) = addAndTrack(this, listener, idRes, addAction, removeAction)

inline fun <T, L, AR, RR> T.addAndTrack(
    view: View,
    listener: L?,
    @IdRes idRes: Int,
    addAction: T.(L) -> AR,
    removeAction: T.(L) -> RR
) {
    ListenerUtil.trackListener(view, listener, idRes)?.also { oldListener -> removeAction(oldListener) }
    if (listener != null) addAction(listener)
}

fun <L> View.getListener(@IdRes idRes: Int): L? = ListenerUtil.getListener<L>(this, idRes)

inline fun <T> ObservableField<T>.check(crossinline predicate: (T?) -> Boolean): ObservableBoolean {
    return object : ReadOnlyObservableBoolean(this) {
        override fun get() = predicate(this@check.get())
    }
}

inline fun ObservableInt.check(crossinline predicate: (Int) -> Boolean): ObservableBoolean {
    return object : ReadOnlyObservableBoolean(this) {
        override fun get() = predicate(this@check.get())
    }
}

private class MapValueCheckingObservableBoolean<K, V>(
    private val map: ObservableMap<K, V>,
    private val predicate: (Map<K, V>) -> Boolean
) : ReadOnlyObservableBoolean() {

    init {
        map.addOnMapChangedCallback(MapDependencyCallback())
    }

    override fun get() = predicate(map)

    private inner class MapDependencyCallback<K, V> : ObservableMap.OnMapChangedCallback<ObservableMap<K, V>, K, V>() {
        override fun onMapChanged(sender: ObservableMap<K, V>?, key: K) {
            notifyChange()
        }
    }
}

fun <K, V> ObservableMap<K, V>.check(predicate: (Map<K, V>) -> Boolean): ObservableBoolean {
    return MapValueCheckingObservableBoolean(this, predicate)
}

fun <K, V> ObservableMap<K, V>.checkKey(key: K, predicate: (V?) -> Boolean): ObservableBoolean {
    return check { map -> predicate(map[key]) }
}

inline fun <T, R> ObservableField<T>.map(crossinline mapper: (T?) -> (R?)): ObservableField<R> {
    return object : ReadOnlyObservableField<R>(this) {
        override fun get() = mapper(this@map.get())
    }
}

inline fun <T> ObservableField<T>.map(crossinline mapper: (T?) -> (Int)): ObservableInt {
    return object : ReadOnlyObservableInt(this) {
        override fun get() = mapper(this@map.get())
    }
}

infix fun ObservableBoolean.or(other: ObservableBoolean): ObservableBoolean {
    return object : ReadOnlyObservableBoolean(this, other) {
        override fun get() = this@or.get() || other.get()
    }
}

infix fun ObservableBoolean.and(other: ObservableBoolean): ObservableBoolean {
    return object : ReadOnlyObservableBoolean(this, other) {
        override fun get() = this@and.get() && other.get()
    }
}

private class ObservableFieldChecker(
    private val predicate: (Array<Any?>) -> Boolean,
    private vararg val observables: ObservableField<*>
) : ReadOnlyObservableBoolean(*observables) {
    override fun get() = predicate(observables.map { it.get() }.toTypedArray())
}

object Observables {
    fun check(vararg observableFields: ObservableField<*>, predicate: (Array<Any?>) -> Boolean): ObservableBoolean {
        return ObservableFieldChecker(predicate, *observableFields)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T1, T2> check(
        field1: ObservableField<T1>,
        field2: ObservableField<T2>,
        predicate: (T1?, T2?) -> Boolean
    ): ObservableBoolean {
        return check(field1, field2) { values ->
            predicate(values[0] as? T1, values[1] as? T2)
        }
    }
}

fun ObservableInt.increment() = set(get() + 1)

fun ObservableInt.decrement() = set(get() - 1)