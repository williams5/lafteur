package com.lafteur.app.helpers.extensions

import android.animation.Animator

import android.view.ViewPropertyAnimator
import androidx.transition.Transition

inline fun ViewPropertyAnimator.setListener(
    crossinline onEnd: (animator: Animator) -> Unit = {},
    crossinline onStart: (animator: Animator) -> Unit = {},
    crossinline onCancel: (animator: Animator) -> Unit = {},
    crossinline onRepeat: (animator: Animator) -> Unit = {}
): ViewPropertyAnimator {
    val listener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animator: Animator) = onRepeat(animator)
        override fun onAnimationEnd(animator: Animator) = onEnd(animator)
        override fun onAnimationCancel(animator: Animator) = onCancel(animator)
        override fun onAnimationStart(animator: Animator) = onStart(animator)
    }
    setListener(listener)
    return this
}

inline fun Transition.addListener(
    crossinline onEnd: (transition: Transition) -> Unit = {},
    crossinline onStart: (transition: Transition) -> Unit = {},
    crossinline onCancel: (transition: Transition) -> Unit = {},
    crossinline onPause: (transition: Transition) -> Unit = {},
    crossinline onResume: (transition: Transition) -> Unit = {}
): Transition {
    val listener = object : Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition)  = onEnd(transition)

        override fun onTransitionResume(transition: Transition) = onResume(transition)

        override fun onTransitionPause(transition: Transition) = onPause(transition)

        override fun onTransitionCancel(transition: Transition) = onCancel(transition)

        override fun onTransitionStart(transition: Transition) = onStart(transition)
    }
    addListener(listener)
    return this
}

