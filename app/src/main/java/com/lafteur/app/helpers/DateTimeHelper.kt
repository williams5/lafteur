package com.lafteur.app.helpers

import android.annotation.SuppressLint
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import org.joda.time.DateTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar

class DateTimeHelper {

    companion object {
        fun buildDatePickerDialog(onDateSet: (date: String) -> Unit): DatePickerDialog {
            val now = Calendar.getInstance()
            val dpd = DatePickerDialog.newInstance(
                { view, year, monthOfYear, dayOfMonth -> onDateSet("$dayOfMonth/$monthOfYear/$year") },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.minDate = Calendar.getInstance()
            dpd.version = DatePickerDialog.Version.VERSION_2
            dpd.isThemeDark = true
            return dpd
        }


        fun buildTimePickerDialog(onTimeSet: (time: String) -> Unit): TimePickerDialog {
            val tpd = TimePickerDialog.newInstance({ view, hourOfDay, minute, second -> onTimeSet("$hourOfDay:$minute") }, true)
            tpd.version = TimePickerDialog.Version.VERSION_2
            tpd.isThemeDark = true
            return tpd
        }

        @SuppressLint("SimpleDateFormat")
        fun generateDateTime(dateString: String, timeString: String): DateTime {
            val dtStart = "${dateString}T${timeString}"
            val format = SimpleDateFormat("dd/MM/yyyy'T'HH:mm")
            try {
                val date = format.parse(dtStart)
                return DateTime(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return DateTime.now()
        }
    }


}