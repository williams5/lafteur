package com.lafteur.app.helpers

import androidx.transition.ChangeBounds
import androidx.transition.ChangeImageTransform
import androidx.transition.ChangeTransform
import androidx.transition.TransitionSet

class CustomTransition : TransitionSet() {

    init {
        ordering = ORDERING_TOGETHER
        addTransition( ChangeBounds())
        addTransition( ChangeTransform())
        addTransition( ChangeImageTransform())


    }

}