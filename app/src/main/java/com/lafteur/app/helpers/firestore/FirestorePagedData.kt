package com.lafteur.app.helpers.firestore

import com.google.firebase.Timestamp

interface FirestorePagedData {
    fun getId(): String
    fun getComparableDate(): Timestamp
}