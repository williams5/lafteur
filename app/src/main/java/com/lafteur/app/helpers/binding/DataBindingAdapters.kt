package com.lafteur.app.helpers.binding

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputLayout
import com.lafteur.app.R
import com.lafteur.app.constants.Enums
import com.lafteur.app.helpers.extensions.getColor
import com.lafteur.app.helpers.extensions.getString
import com.lafteur.app.helpers.extensions.setListener
import com.lafteur.app.helpers.extensions.toHumanDate
import org.joda.time.DateTime
import java.sql.Timestamp

@BindingAdapter("android:visible")
fun View.changeVisible(visible: Boolean) {
    if (isVisible != visible) isVisible = visible
}

@BindingAdapter("android:invisible")
fun View.changeInvisible(invisible: Boolean) {
    if (isInvisible != invisible) isInvisible = invisible
}

@BindingAdapter("android:enabled")
fun View.changeEnabled(enabled: Boolean) {
    if (isEnabled != enabled) {
        isEnabled = enabled
    }
}

@BindingAdapter("android:activated")
fun View.changeActivated(activated: Boolean) {
    if (isActivated != activated) isActivated = activated
}

interface OnTextSentListener {
    fun onTextSent()
}

@BindingAdapter("android:onTextSent")
fun EditText.setOnTextSentListener(listener: OnTextSentListener?) {
    setOnEditorActionListener { _, actionId, event ->
        if (listener != null
            && (actionId == EditorInfo.IME_ACTION_SEND
                || event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER)
        ) {
            listener.onTextSent()
            true
        } else {
            false
        }
    }
}

@BindingAdapter("app:setBackgroundDrawable")
fun View.setBackgroundDrawable(@DrawableRes drawable: Int) {
    background = ContextCompat.getDrawable(context, drawable)
}

interface OnTabSelectedListener {
    fun onTabSelected(index: Int)
}

@BindingAdapter("app:fadeVisibility")
fun View.fadeVisibility(isVisible: Boolean) {
    animate()
        .alpha(if (isVisible) 1f else 0f)
        .setListener(
            onStart = {
                if (isVisible) this.isVisible = true
            },
            onEnd = {
                if (!isVisible) this.isVisible = false
            }
        )
        .start()
}

@BindingAdapter("android:background")
fun View.setBackground(@DrawableRes drawable: Int) {
    background = ContextCompat.getDrawable(this.context, drawable)
}

@BindingAdapter("android:drawableStart")
fun TextView.setDrawableStart(@DrawableRes drawable: Int) {
    setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this.context, drawable), null, null, null)
}

@BindingAdapter("android:tint")
fun ImageView.setTint(@ColorRes color: Int) {
    setColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.MULTIPLY)
}

@BindingAdapter("app:src")
fun ImageView.setSrc(@DrawableRes drawableRes: Int) {
    setImageDrawable(ContextCompat.getDrawable(context, drawableRes))
}

interface ImageLoadedListener {
    fun onImageLoaded(failed: Boolean)
}

@BindingAdapter("app:urlImage", "app:roundedRadius", "app:onImageLoaded", requireAll = false)
fun ImageView.loadImage(urlImage: String?, roundedRadius: Float?, imageLoadedListener: ImageLoadedListener?) {
    if (urlImage == null) setBackgroundColor(getColor(R.color.placeholder_color)) else {
        val listener = object : RequestListener<Drawable> {

            override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                imageLoadedListener?.onImageLoaded(true)
                return false
            }

            override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: com.bumptech.glide.load.DataSource?, isFirstResource: Boolean): Boolean {
                imageLoadedListener?.onImageLoaded(false)
                return false
            }

        }
        val requestBuilder = Glide.with(this)
            .load(urlImage)
            .listener(listener)


        roundedRadius?.let {
            requestBuilder.apply(RequestOptions.bitmapTransform(RoundedCorners(roundedRadius.toInt())))
            requestBuilder.apply(RequestOptions.placeholderOf(GradientDrawable().apply {
                this.cornerRadius = roundedRadius
                this.setColor(getColor(R.color.placeholder_color))
            }))
        }

        requestBuilder.into(this)
    }
}

@BindingAdapter(value = ["app:textResId", "app:textParams"], requireAll = false)
fun TextView.setTextResId(@StringRes textResId: Int, textParams: Array<Any?>?) {
    when {
        textResId == 0 -> text = null
        textParams.isNullOrEmpty() -> setText(textResId)
        else -> text = context.getString(textResId, *textParams)
    }
}

@BindingAdapter("app:errorResId")
fun TextInputLayout.setErrorResId(@StringRes errorResId: Int) {
    error = if (errorResId == 0) null else getString(errorResId)
}

interface OnPageChangedListener {
    fun onPageChanged(position: Int)
}

@BindingAdapter("app:onPageChanged")
fun ViewPager.addPageChangedListener(listener: OnPageChangedListener?) {
    addAndTrack(
        if (listener == null) {
            null
        } else {
            object : ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    listener.onPageChanged(position)
                }
            }
        },
        R.id.itemDecoration,
        ViewPager::addOnPageChangeListener,
        ViewPager::removeOnPageChangeListener
    )
}

@BindingAdapter("app:refreshing")
fun SwipeRefreshLayout.changeRefreshing(refreshing: Boolean) {
    if (isRefreshing != refreshing) {
        isRefreshing = refreshing
    }
}

@BindingAdapter("app:selectedItem", requireAll = false)
fun BottomNavigationView.setSelectedItem(position: Int) {
    menu.getItem(position).isChecked = true
}

@BindingMethods(
    BindingMethod(
        type = SwipeRefreshLayout::class,
        attribute = "app:onRefresh",
        method = "setOnRefreshListener"
    ),
    BindingMethod(
        type = BottomNavigationView::class,
        attribute = "app:onItemSelected",
        method = "setOnNavigationItemSelectedListener"
    )
)
object DataBindingAdapters

///CUSTOM


@BindingAdapter("app:date")
fun TextView.setDateHuman(date: DateTime?) {
    if (date == null) isVisible = false
    else
        text = text.toString().plus(date.toHumanDate())
}

@BindingAdapter("app:date")
fun TextView.setDateHumanFromTimeStamp(timestamp: com.google.firebase.Timestamp?) {
    if (timestamp == null) isVisible = false
    else
        text = timestamp.toHumanDate()
}

@BindingAdapter("app:stateRequest")
fun TextView.setBackgroundStateRequest(state: Enums.StateUserRequest) {
    val gradientDrawable = background as GradientDrawable
    when(state){
        Enums.StateUserRequest.ACCEPTED -> {
            gradientDrawable.setColor(getColor(R.color.state_user_accepted))
            setText(R.string.state_accepted)
        }
        Enums.StateUserRequest.DENIED -> {
            gradientDrawable.setColor(getColor(R.color.state_user_denied))
            setText(R.string.state_denied)
        }
        Enums.StateUserRequest.WAITING -> {
            gradientDrawable.setColor(getColor(R.color.state_user_waiting))
            setText(R.string.state_waiting)
        }
    }


}
