/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.helpers.extensions

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import android.view.animation.TranslateAnimation
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.getColor(@ColorRes color: Int): Int {
    return ContextCompat.getColor(context, color)
}

fun View.slideUp(duration: Int = 500) {
    this.visibility = View.VISIBLE
    val animate = TranslateAnimation(0f, 0f, this.height.toFloat(), 0f)
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun View.slideDown(duration: Int = 500) {
    this.visibility = View.GONE
    val animate = TranslateAnimation(0f, 0f, 0f, this.height.toFloat())
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun View.setTint(@ColorRes colorRes: Int) {
    ViewCompat.setBackgroundTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, colorRes)))
}

fun View.setTint(color: String) {

    if (!color.startsWith("#")) {
        ViewCompat.setBackgroundTintList(this, ColorStateList.valueOf(Color.parseColor("#$color")))
    } else {
        ViewCompat.setBackgroundTintList(this, ColorStateList.valueOf(Color.parseColor(color)))
    }

}

fun View.getString(@StringRes resId: Int): String = context.getString(resId)

fun View.getString(@StringRes resId: Int, vararg args: Any): String = context.getString(resId, *args)

fun View.getDimensionPx(@DimenRes dimens: Int):Int {
    return context.getDimensionPx(dimens)
}