package com.lafteur.app.helpers.extensions

import android.graphics.Bitmap
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.lafteur.app.R
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView

fun Fragment.launchCropImage(){
    CropImage.activity()
        .setAspectRatio(319, 248)
        .setRequestedSize(720,600, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
        .setFixAspectRatio(true)
        .setMultiTouchEnabled(false)
        .setOutputCompressFormat(Bitmap.CompressFormat.WEBP)
        .setOutputCompressQuality(80)
        .setBackgroundColor(ContextCompat.getColor(this.requireActivity(), R.color.transparent_salmon))
        .setActivityMenuIconColor(ContextCompat.getColor(this.requireActivity(), R.color.salmon))
        .setGuidelines(CropImageView.Guidelines.ON)
        .start(this.requireContext(), this)
}