package com.lafteur.app.helpers.firestore

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.helpers.livedata.MutableLiveList
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.infrastructure.event.Event
import com.lafteur.app.infrastructure.event.EventQueryFactory
import com.orhanobut.logger.Logger


class FirestorePagedDataSourceImpl<T : FirestorePagedData> private constructor(initialQuery: Query, private val maxItemPerPage: Int, private val clazz: Class<T>) : FirestorePagedDataSource {

    companion object {
        fun <T : FirestorePagedData> create(eventQueryFactory: EventQueryFactory): FirestorePagedDataSourceImpl<Event> {
            return FirestorePagedDataSourceImpl(eventQueryFactory.getInitialQuery(), eventQueryFactory.getLimitItemPerPage(), eventQueryFactory.getClassModel())
        }
    }

    private val TIME_FIRST_LOAD: Long = 2000
    private val listListenerPagedEvent = mutableListOf<ListenerRegistration>()
    private var lastVisibleEvent: Timestamp? = null
    private var query = initialQuery

    val liveLoadindState = MutableLiveData<ProgressStatus>()
    val listLive: MutableLiveList<T> = MutableLiveList()
    val hasNextPage = MutableLiveData<Boolean>()

    override fun loadInitial() {
        Logger.d("Load initial")
        liveLoadindState.changeValue(ProgressStatus.FIRST_LOADING)
        listListenerPagedEvent.add(createQuery())

    }

    override fun loadNext() {
        Logger.d("Load next")
        query = query.startAfter(lastVisibleEvent)
        liveLoadindState.changeValue(ProgressStatus.LOADING)
        listListenerPagedEvent.add(createQuery())
    }

    override fun clear() {
        /*
        query = eventRepository.getQueryPagedCollection()
        isLastEventReached = false
        lastVisibleEvent = null

         */
        listListenerPagedEvent.forEach {
            it.remove()
        }
        Logger.d("clear list listener")
    }


    private fun createQuery(): ListenerRegistration {
        return query.addSnapshotListener { querySnapshot, firebaseFirestoreException ->
            var needSort = false
            if (firebaseFirestoreException != null) {
                Logger.e(firebaseFirestoreException.localizedMessage
                    ?: firebaseFirestoreException.toString())
                return@addSnapshotListener
            }
            querySnapshot?.documentChanges?.forEachIndexed { index, documentChange ->
                val item = documentChange.document.toObject(clazz)
                Logger.d("document ${item.getId()} is ${documentChange.type.name}")
                when (documentChange.type) {
                    DocumentChange.Type.ADDED -> {
                        listLive.add(item)
                        if (needSort) {
                            listLive.sortBy { it.getComparableDate() }
                            listLive.refresh()
                            needSort = false
                        }
                    }
                    DocumentChange.Type.MODIFIED -> listLive[listLive.indexOfFirst { it.getId() == item.getId() }] = item
                    DocumentChange.Type.REMOVED -> {
                        needSort = true //in case a new document  added
                        listLive.removeAny { it.getId() == item.getId() }
                    }
                }
            }
            needSort = false

            querySnapshot?.size()?.let { querySnapshotSize ->
                Logger.d("load $querySnapshotSize document")
                if (querySnapshotSize < maxItemPerPage) {
                    hasNextPage.changeValue(false)
                } else {
                    hasNextPage.changeValue(true)
                    val lastVisibleEvent = querySnapshot.documents[querySnapshotSize - 1]
                    this.lastVisibleEvent = listLive.last().getComparableDate()
                    Logger.d("NEW Last visible = ${lastVisibleEvent.id}")
                }
            }

            if (liveLoadindState.value == ProgressStatus.FIRST_LOADING) {
                Handler().postDelayed({
                    liveLoadindState.changeValue(ProgressStatus.LOADED)
                }, TIME_FIRST_LOAD)
            } else
                liveLoadindState.changeValue(ProgressStatus.LOADED)

        }

    }

}