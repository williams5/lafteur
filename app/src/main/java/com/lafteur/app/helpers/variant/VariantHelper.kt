/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.helpers.variant

interface VariantHelper {

    fun getBackendEndPoint(): String
}