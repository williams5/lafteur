package com.lafteur.app.helpers.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlin.reflect.KClass



class KeyedViewModelLazy<VM : ViewModel>(
    private val fragment: Fragment,
    private val fromActivity: Boolean,
    private val viewModelClass: KClass<VM>,
    private val viewModelFactory: ViewModelProvider.Factory
) : Lazy<VM> {
    private var cached: VM? = null

    override val value: VM
        get() {
            return cached
                ?: if (fromActivity)
                    ViewModelProvider(fragment.requireActivity() as AppCompatActivity).get(viewModelClass.java).also {
                        cached = it
                    }
                else
                    ViewModelProvider(fragment, viewModelFactory).get(viewModelClass.java).also {
                        cached = it
                    }
        }

    override fun isInitialized() = cached != null
}