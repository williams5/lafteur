package com.lafteur.app.ui.home.feed

import android.os.Bundle
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.lafteur.app.R
import com.lafteur.app.core.list.BaseListAdapter
import com.lafteur.app.core.list.ListFragment
import com.lafteur.app.core.list.base.ListUIConfig
import com.lafteur.app.databinding.FragmentHomeFeedBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.di.DaggerHomeComponent
import com.lafteur.app.ui.home.models.FeedEventModel
import com.orhanobut.logger.Logger


class FeedFragment : ListFragment<FeedEventModel, FeedViewModel, FragmentHomeFeedBinding>() {

    override val contentId: Int = R.layout.fragment_home_feed

    override val listViewModel: FeedViewModel by lazy { getViewModelFragment<FeedViewModel>() }

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override val recyclerView: RecyclerView?
        get() = dataBinding?.rvEvents

    override fun setupDataBinding() {
        super.setupDataBinding()
        dataBinding?.activityVM = activityViewModel
        Logger.d("setup data frag")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerHomeComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(activity))
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)


        listViewModel.liveOpenDetailEvent.observe(this, Observer { positionFeedEvent ->
            recyclerView?.layoutManager?.findViewByPosition(positionFeedEvent.first)?.let {
                activityViewModel.selectedEvent.set(positionFeedEvent.second)
                val img = it.findViewById<ImageView>(R.id.iv_event)
                goToFragmentWithAnimation(R.id.go_to_detailEventFragment, Pair(img, img.transitionName))
            }
        })

        Logger.d("create frag")
    }

    override fun onResume() {
        super.onResume()
        Logger.d("resume frag")
    }

    override fun onStop() {
        super.onStop()
        Logger.d("stop frag")
    }

    override fun onDestroy() {
        super.onDestroy()
        Logger.d("destroy frag")

    }

    override fun createUIConfig(): ListUIConfig<FeedEventModel, FeedViewModel> =
        object : ListUIConfig<FeedEventModel, FeedViewModel>(this) {
            override val layoutId: Int = R.layout.fragment_home_feed
        }

    override fun createAdapter(): BaseListAdapter<FeedEventModel, FeedViewModel, FragmentHomeFeedBinding> =
        object : BaseListAdapter<FeedEventModel, FeedViewModel, FragmentHomeFeedBinding>(this) {
            override fun getCellLayoutId(viewType: Int): Int = R.layout.item_event

            override fun createItemViewModel(item: FeedEventModel, index: Int, type: Int): Any {
                return FeedItemViewModel(item, index, viewModel)
            }
        }

}