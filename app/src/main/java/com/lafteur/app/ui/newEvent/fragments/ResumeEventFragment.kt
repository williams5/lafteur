package com.lafteur.app.ui.newEvent.fragments

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventResumeBinding
import com.lafteur.app.helpers.extensions.gone
import com.lafteur.app.helpers.extensions.hide
import com.lafteur.app.helpers.extensions.load
import com.lafteur.app.ui.newEvent.NewEventActivity
import com.lafteur.app.ui.newEvent.NewEventViewModel
import kotlinx.android.synthetic.main.activity_new_event.*
import kotlinx.android.synthetic.main.fragment_new_event_resume.*

class ResumeEventFragment : BaseFragment<FragmentNewEventResumeBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_resume

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as NewEventActivity).fullscreenFrag(true)
        activityViewModel.onEventCreated().observe(this, eventCreatedObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iv_event.load(Uri.parse(activityViewModel.eventModel.pictureUri!!))
        tv_name.text = activityViewModel.eventModel.owner?.name
        tv_city.text = activityViewModel.eventModel.adressEvent
        val fullDateBegin = activityViewModel.eventModel.dateBegin!!.toString("d MMMM HH:mm")
        val fullDateEnd = activityViewModel.eventModel.dateEnd!!.toString("d MMMM HH:mm")
        tv_date_begin.text = getString(R.string.resume_event_date_begin, fullDateBegin)
        tv_date_end.text = getString(R.string.resume_event_date_begin, fullDateEnd)
        tv_nb_current_guest.text = activityViewModel.eventModel.nbCurrentGuest.toString()
        tv_nb_accepted_guest.text = activityViewModel.eventModel.nbGuestAccepted.toString()
        tv_description.text = activityViewModel.eventModel.description
        tv_address.text = activityViewModel.eventModel.adressEvent
        tv_phone.text = activityViewModel.eventModel.phoneOwner

        btn_update.setOnClickListener {
            findNavController().navigate(R.id.go_to_updateEventFragment)
        }

        btn_close.setOnClickListener {
            activity.showProgressDialog(true)
            activityViewModel.createEvent()
        }

        if (activityViewModel.eventModel.idEvent != null)
            requireActivity().onBackPressedDispatcher.addCallback {
                activity.finish()
            }


    }

    override fun onResume() {
        super.onResume()
        (activity as NewEventActivity).toolbar.gone()

    }

    override fun onStop() {
        super.onStop()
        (activity as NewEventActivity).toolbar.showBack()
        (activity as NewEventActivity).fullscreenFrag(false)
    }

    private val eventCreatedObserver = Observer<Boolean> { eventCreated ->
        if (eventCreated) {
            activity.hideProgressDialog()
            activity.finish()
        } else {
            activity.showErrorFlashBar("Malheureusement votre 'évenèment n'a pu être créée ,blablablablalblalbla!")
        }
    }

}