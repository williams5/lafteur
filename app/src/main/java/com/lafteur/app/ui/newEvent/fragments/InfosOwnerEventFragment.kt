package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventInfosOwnerBinding
import com.lafteur.app.ui.newEvent.NewEventViewModel
import kotlinx.android.synthetic.main.fragment_new_event_infos_owner.*

class InfosOwnerEventFragment : BaseFragment<FragmentNewEventInfosOwnerBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_infos_owner

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityViewModel.eventModel.phoneOwner?.let {
            et_phone.text = it
        }

        activityViewModel.eventModel.adressEvent?.let {
            et_address.text = it
        }

        btn_next.setOnClickListener {
            activityViewModel.updateEventModel(phoneOwner = et_phone.text, addressEvent = et_address.text)
            findNavController().navigate(R.id.go_to_resumeEventFragment)
        }

    }

}