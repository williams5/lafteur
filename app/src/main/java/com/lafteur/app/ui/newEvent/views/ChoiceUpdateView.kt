package com.lafteur.app.ui.newEvent.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.lafteur.app.R
import kotlinx.android.synthetic.main.view_choice_update.view.*

class ChoiceUpdateView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : LinearLayout(context, attrs) {

    var listener: ChoiceUpdateViewListener? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.view_choice_update, this, true)

        attrs?.let { attributeSet ->
            val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.ChoiceUpdateView, 0, 0)

            typedArray.getString(R.styleable.ChoiceUpdateView_text)?.let {
                tv_choice.text = it
            }

            val icon = typedArray.getDrawable(R.styleable.ChoiceUpdateView_icon)

            iv_choice.setImageDrawable(icon)

            typedArray.recycle()
        }

        setOnClickListener {
            listener?.onCliked()
        }
    }

}

interface ChoiceUpdateViewListener {
    fun onCliked()
}
