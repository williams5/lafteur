/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.ui.shared

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.Toolbar
import com.lafteur.app.R
import com.lafteur.app.helpers.extensions.gone
import com.lafteur.app.helpers.extensions.show
import kotlinx.android.synthetic.main.view_toolbar_default.view.*

class DefaultToolbar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : Toolbar(context, attrs) {


    init {
        LayoutInflater.from(context).inflate(R.layout.view_toolbar_default, this, true)

        btn_back.setOnClickListener {
            (context as Activity).onBackPressed()
        }

        setContentInsetsAbsolute(0, 0)
        setContentInsetsRelative(0, 0)
    }

    fun hideBack(){
        btn_back.gone()
    }

    fun showBack(){
        btn_back.show()
    }


}

