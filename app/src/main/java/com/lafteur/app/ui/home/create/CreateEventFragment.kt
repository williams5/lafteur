package com.lafteur.app.ui.home.create

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.lafteur.app.R
import com.lafteur.app.constants.Constants
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentHomeCreateEventBinding
import com.lafteur.app.helpers.DateTimeHelper
import com.lafteur.app.helpers.extensions.launchCropImage
import com.lafteur.app.helpers.extensions.startActivityWithBundle
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.newEvent.NewEventActivity
import com.orhanobut.logger.Logger
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_home_create_event.*

class CreateEventFragment : BaseFragment<FragmentHomeCreateEventBinding>() {

    override val contentId: Int = R.layout.fragment_home_create_event

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_picture.setOnClickListener {
            launchCropImage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val result = CropImage.getActivityResult(data)
                        val bundle = Bundle()
                        bundle.putString(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT, result.uri.toString())
                        activity.startActivityWithBundle<NewEventActivity>(bundle, false)
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {

                    }
                    else -> {

                    }
                }
            }
        }
    }

}