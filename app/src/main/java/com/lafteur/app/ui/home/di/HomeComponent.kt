package com.lafteur.app.ui.home.di

import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.di.ViewModelModule
import com.lafteur.app.ui.home.HomeActivity
import com.lafteur.app.ui.home.detail.DetailEventFragment
import com.lafteur.app.ui.home.feed.FeedFragment
import com.lafteur.app.ui.home.myEvent.HomeMyEventFragment
import com.lafteur.app.ui.home.myEvent.event.MyEventRequestFragment
import com.lafteur.app.ui.home.myEvent.request.RequestEventFragment
import dagger.Component

@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface HomeComponent {
    fun inject(homeActivity: HomeActivity)
    fun inject(detailEventFragment: DetailEventFragment)
    fun inject(feedFragment: FeedFragment)
    fun inject(homeMyEventFragment: HomeMyEventFragment)
    fun inject(requestEventFragment: RequestEventFragment)
    fun inject(myEventRequestFragment: MyEventRequestFragment)

}
