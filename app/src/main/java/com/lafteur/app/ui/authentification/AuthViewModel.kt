package com.lafteur.app.ui.authentification

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.login.LoginResult
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.lafteur.app.constants.Constants
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.infrastructure.facebook.FacebookUserDto
import com.lafteur.app.infrastructure.facebook.FacebookUserRepository
import com.lafteur.app.infrastructure.user.User
import com.lafteur.app.infrastructure.user.UserRepository
import com.orhanobut.logger.Logger
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AuthViewModel @Inject constructor(private val userRepository: UserRepository,
                                        private val facebookUserRepository: FacebookUserRepository) : BaseViewModel() {

    private val firebaseAuth = FirebaseAuth.getInstance()
    var storedVerificationId: String? = ""
    var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    var currentUser: User? = null
    var number: String = ""

    private val liveSatusLogin = MutableLiveData<Enums.SatusLogin>()
    private val liveVerificationStateChanged = MutableLiveData<Enums.OnVerificationStateChanged>()

    fun onVerificationStateChanged(): LiveData<Enums.OnVerificationStateChanged> {
        return liveVerificationStateChanged
    }


    fun onStatusLoginChanged(): LiveData<Enums.SatusLogin> {
        return liveSatusLogin
    }

    fun onCurrentUserLoaded(): LiveData<User> {
        return userRepository.getLiveUser(firebaseAuth.currentUser!!.uid)
    }

    fun createUser(name: String, birthday: String) {
       updateInfosUser(name, birthday)
        userRepository.createUser(currentUser!!).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Logger.d("user is create")
            } else {
                Logger.d("user is not create")
            }
        }
    }

    private fun updateInfosUser(name: String, birthday: String) {
        currentUser!!.name = name
        currentUser!!.birthday = birthday
    }

    fun handleFacebookLogin(loginResult: LoginResult) {
        val credential = FacebookAuthProvider.getCredential(loginResult.accessToken.token)
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener { taskResult ->
                if (taskResult.isSuccessful) {
                    currentUser = User(firebaseAuth.currentUser!!.uid,Constants.LOGIN_BY_FB)
                    userRepository.isCurrentUserLogged()
                        .flatMap { isCurrentUserLogged ->
                            if (isCurrentUserLogged) {
                                Single.just(true)

                            } else {
                                facebookUserRepository.getInfosUser()
                                    .subscribeOn(Schedulers.io())
                            }
                        }
                        .subscribeBy(
                            onSuccess = { result ->
                                if (result is FacebookUserDto) {
                                    updateInfosUser(result.firstName, result.birthday)
                                    liveSatusLogin.postValue(Enums.SatusLogin.NOT_REGISTERED)
                                } else {
                                    liveSatusLogin.postValue(Enums.SatusLogin.FB_LOGIN_SUCCESS)
                                }
                            },
                            onError = {
                                liveSatusLogin.postValue(Enums.SatusLogin.LOGIN_FAILED)
                                Logger.e(it.localizedMessage)
                            }
                        ).addTo(compositeDisposable)
                }
            }
    }

    fun startPhoneVerification(activity: AppCompatActivity) {
        PhoneAuthProvider.getInstance()
            .verifyPhoneNumber(
                number,
                Constants.TIMEOUT_NUMBER_VERIF,
                TimeUnit.SECONDS,
                activity,
                callbacks,
                resendToken
            )
    }

    fun handlePhoneLogin(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Logger.d("success")
                    currentUser = User(firebaseAuth.currentUser!!.uid,Constants.LOGIN_BY_PHONE)
                    currentUser!!.phone = firebaseAuth.currentUser!!.phoneNumber!!
                    userRepository.isCurrentUserLogged()
                        .subscribeBy(
                            onSuccess = { isCurrentUserLogged ->
                                if (isCurrentUserLogged) {
                                    liveSatusLogin.postValue(Enums.SatusLogin.PHONE_LOGIN_SUCCESS)
                                } else {
                                    liveSatusLogin.postValue(Enums.SatusLogin.NOT_REGISTERED)
                                }
                            }
                        )

                } else {
                    Logger.w("${task.exception!!.localizedMessage}}")
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        Logger.e("code invalide")
                    }
                    Logger.e("Erreur réessayer plus tard")

                }
            }
    }

    private val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


        override fun onVerificationCompleted(credential: PhoneAuthCredential) {
            Logger.d("$credential")
            liveVerificationStateChanged.postValue(Enums.OnVerificationStateChanged.ON_COMPLETED)
            handlePhoneLogin(credential)

        }

        override fun onVerificationFailed(e: FirebaseException) {
            Logger.e(e.localizedMessage)

            if (e is FirebaseAuthInvalidCredentialsException) {
                liveVerificationStateChanged.postValue(Enums.OnVerificationStateChanged.ON_FAILED)
                // activity.toast("Numéro de téléphone incorrect")
            } else if (e is FirebaseTooManyRequestsException) {
                liveVerificationStateChanged.postValue(Enums.OnVerificationStateChanged.ON_QUOTA_EXCEED)
                // The SMS quota for the project has been exceeded
                /*
                Snackbar.make(activity.findViewById(android.R.id.content),
                    "Quota exceeded.",
                    Snackbar.LENGTH_SHORT).show()
                    */
            }
        }

        override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
            Logger.d(verificationId)
            // Save verification ID and resending token so we can use them later
            storedVerificationId = verificationId
            resendToken = token
            liveVerificationStateChanged.postValue(Enums.OnVerificationStateChanged.ON_CODE_SENT)
        }


    }


}