package com.lafteur.app.ui.authentification.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentAuthPhoneBinding
import com.lafteur.app.helpers.extensions.showKeyboard
import com.lafteur.app.ui.authentification.AuthViewModel
import kotlinx.android.synthetic.main.fragment_auth_phone.*
import org.jetbrains.anko.toast
import kotlin.reflect.KClass

class PhoneFragment : BaseFragment<FragmentAuthPhoneBinding>() {

    override val contentId: Int = R.layout.fragment_auth_phone

    private val activityViewModel: AuthViewModel by lazy { getViewModelActivity<AuthViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ccp.registerCarrierNumberEditText(et_phone)

        btn_next.setOnClickListener {
            btn_next.startAnimation()
            if (!ccp.isValidFullNumber) {
                btn_next.revertAnimation()
                activity.toast("Vous devez rentrez un numéro de téléphone valide")
            } else {
                activityViewModel.number = ccp.fullNumberWithPlus
                activityViewModel.startPhoneVerification(activity)
            }
        }
        activityViewModel.onVerificationStateChanged().observe(viewLifecycleOwner, verificationStateChangedObserver)
    }

    private val verificationStateChangedObserver = Observer<Enums.OnVerificationStateChanged> {
        when (it) {
            Enums.OnVerificationStateChanged.ON_COMPLETED -> {

            }
            Enums.OnVerificationStateChanged.ON_FAILED -> {
                activity.toast("Veuillez entrer un numéro de télphone valide")
            }
            Enums.OnVerificationStateChanged.ON_QUOTA_EXCEED -> {
                activity.toast("Quota exceeded")
            }
            Enums.OnVerificationStateChanged.ON_CODE_SENT -> {
                btn_next.revertAnimation {
                    goToFragmentWithAnimation(R.id.go_to_verifPhoneFragment, btn_next to btn_next.transitionName)
                    activity.supportActionBar?.show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        et_phone.requestFocus()
        et_phone.showKeyboard()
    }



}