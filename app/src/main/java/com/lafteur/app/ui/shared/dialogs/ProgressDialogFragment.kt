/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.ui.shared.dialogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.lafteur.app.R
import com.lafteur.app.core.BaseActivity

class ProgressDialogFragment : DialogFragment() {
    lateinit var mContext: Context

    private var hasDim = true

    companion object {
        fun createDialog(): ProgressDialogFragment {
            val frag = ProgressDialogFragment()
            return frag
        }
    }

    fun show(activity: AppCompatActivity, tag: String) {
        super.show(activity.supportFragmentManager, tag)
    }

    fun showWithDim(activity: AppCompatActivity, tag: String) {
        hasDim = false
        super.show(activity.supportFragmentManager, tag)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.ProgressDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        if (hasDim) {
            dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        }
        return inflater.inflate(R.layout.dialog_progress, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
    }

}
