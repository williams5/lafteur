package com.lafteur.app.ui.home.myEvent.request


interface RequestEventItemActionListener {
    fun openEvent(requestEventModel: RequestEventModel, position: Int)
    fun longPressEvent(requestEventModel: RequestEventModel, position: Int)
    fun onImageLoaded()
}

class RequestEventItemViewModel(val event: RequestEventModel, private val position: Int, private val actionListener: RequestEventItemActionListener) {

    fun onImageLoaded() {
        if (position == 1)
            actionListener.onImageLoaded()
    }

    fun openEvent() {
        actionListener.openEvent(event, position)
    }

    fun longPressEvent():Boolean {
        actionListener.longPressEvent(event,position)
        return true
    }

}