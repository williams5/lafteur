package com.lafteur.app.ui.authentification.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentAuthSaveInfoUserBinding
import com.lafteur.app.helpers.extensions.startActivity
import com.lafteur.app.infrastructure.user.User
import com.lafteur.app.ui.authentification.AuthViewModel
import com.lafteur.app.ui.home.HomeActivity
import com.lafteur.app.ui.shared.DefaultEditText
import kotlinx.android.synthetic.main.fragment_auth_save_info_user.*
import org.jetbrains.anko.toast
import java.util.Calendar
import kotlin.reflect.KClass

class SaveInfoUserFragment : BaseFragment<FragmentAuthSaveInfoUserBinding>() {

    override val contentId: Int = R.layout.fragment_auth_save_info_user

    private val activityViewModel: AuthViewModel by lazy { getViewModelActivity<AuthViewModel>() }

    private var current = ""
    private val ddmmyyyy = "DDMMYYYY"
    private val cal = Calendar.getInstance()

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityViewModel.onCurrentUserLoaded().observe(viewLifecycleOwner, currentUserObserver)
        activityViewModel.currentUser?.let {
            et_first_name.text = it.name
            et_date.text = it.birthday
        }

        btn_confirm.setOnClickListener {
            if (et_first_name.isValid() && et_date.isValid()) {
                activityViewModel.createUser(et_first_name.text, et_date.text)
            } else {
                activity.toast("Vous devez remplir correctement tous champs")
            }
        }

        et_date.listener = object : DefaultEditText.DefaultEditTextListener {
            override fun onImeActionDone() {

            }

            override fun afterTextChanged(text: String) {
                formatEtDate(text)
            }

        }
    }

    private val currentUserObserver = Observer<User> { currentUser ->
        currentUser?.let {
            activity.startActivity<HomeActivity>()
        }
    }

    private fun formatEtDate(text: String) {
        if (text != current) {
            var clean = text.replace("[^\\d.]|\\.".toRegex(), "")
            val cleanC = current.replace("[^\\d.]|\\.", "")

            val cl = clean.length
            var sel = cl
            var i = 2
            while (i <= cl && i < 6) {
                sel++
                i += 2
            }
            //Fix for pressing delete next to a forward slash
            if (clean == cleanC) sel--

            if (clean.length < 8) {
                clean += ddmmyyyy.substring(clean.length)
            } else {
                //This part makes sure that when we finish entering numbers
                //the date is correct, fixing it otherwise
                var day = Integer.parseInt(clean.substring(0, 2))
                var mon = Integer.parseInt(clean.substring(2, 4))
                var year = Integer.parseInt(clean.substring(4, 8))

                mon = if (mon < 1) 1 else if (mon > 12) 12 else mon
                cal.set(Calendar.MONTH, mon - 1)
                year = if (year < 1900) 1900 else if (year > 2100) 2100 else year
                cal.set(Calendar.YEAR, year)
                // ^ first set year for the line below to work correctly
                //with leap years - otherwise, date e.g. 29/02/2012
                //would be automatically corrected to 28/02/2012

                day = if (day > cal.getActualMaximum(Calendar.DATE)) cal.getActualMaximum(Calendar.DATE) else day
                clean = String.format("%02d%02d%02d", day, mon, year)
            }

            clean = String.format("%s/%s/%s", clean.substring(0, 2),
                clean.substring(2, 4),
                clean.substring(4, 8))

            sel = if (sel < 0) 0 else sel
            current = clean
            et_date.text = current
            et_date.placeCursorAt(if (sel < current.count()) sel else current.count())
        }
    }

}