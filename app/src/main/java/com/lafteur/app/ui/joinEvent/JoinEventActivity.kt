package com.lafteur.app.ui.joinEvent

import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.lafteur.app.R
import com.lafteur.app.constants.Constants
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.databinding.ActivityJoinEventBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.infrastructure.user.User
import com.lafteur.app.ui.joinEvent.di.DaggerJoinEventComponent
import kotlinx.android.synthetic.main.activity_join_event.*

class JoinEventActivity : BaseActivity<JoinEventViewModel, ActivityJoinEventBinding>(JoinEventViewModel::class, R.layout.activity_join_event) {

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerJoinEventComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        super.onCreate(savedInstanceState)

        val pictureUri = intent.getStringExtra(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT)
        val idEvent = intent.getStringExtra(Constants.BUNDLE_ID_EVENT)
        viewModel.descriptionEvent = intent.getStringExtra(Constants.BUNDLE_DESCRIPTION_EVENT)

        viewModel.updateJoinEventModel(pictureUri = pictureUri, idEvent = idEvent)
        viewModel.onUserLoaded().observe(this, userObserver)
    }

    private val userObserver = Observer<User> {
        com.orhanobut.logger.Logger.d(it)
        val phone = if (it.phone.isEmpty()) {
            null
        } else {
            it.phone
        }
        viewModel.updateJoinEventModel(owner = it, phoneOwner = phone)
        viewModel.onUserLoaded().removeObservers(this)

    }

    fun fullscreenFrag(fullscreen: Boolean) {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cl_join_event)
        constraintSet.clear(nav_host_join_event_fragment.id, ConstraintSet.TOP)
        if (fullscreen) {
            constraintSet.connect(nav_host_join_event_fragment.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
        } else {
            constraintSet.connect(nav_host_join_event_fragment.id, ConstraintSet.TOP, toolbar.id, ConstraintSet.BOTTOM)
        }

        val transition = ChangeBounds()
        transition.duration = 300
        TransitionManager.beginDelayedTransition(cl_join_event, transition)
        constraintSet.applyTo(cl_join_event)
    }

    override fun onBackPressed() {
        if( NavHostFragment.findNavController(nav_host_join_event_fragment).currentDestination?.id == R.id.resumeJoinEventFragment){
            finish()
        }
        else
        super.onBackPressed()
    }

}
