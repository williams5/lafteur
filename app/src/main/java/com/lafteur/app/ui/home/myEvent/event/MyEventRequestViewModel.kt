package com.lafteur.app.ui.home.myEvent.event

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.list.ListViewModel
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.call
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.infrastructure.event.Event
import com.lafteur.app.infrastructure.event.EventRepository
import com.lafteur.app.infrastructure.event.UserRequest
import com.lafteur.app.ui.newEvent.models.EventModel
import org.joda.time.DateTime
import javax.inject.Inject

class MyEventRequestViewModel @Inject constructor(eventRepository: EventRepository) : ListViewModel<MyEventRequestModel>(), MyEventRequestItemActionListener {


    val liveOpenDetailRequest = SingleLiveEvent<Pair<Int, MyEventRequestModel>>()

    val liveImageLoaded = SingleLiveEvent<Unit>()

    val liveSeeMyAfter = SingleLiveEvent<Unit>()

    val liveCreateAfter = SingleLiveEvent<Unit>()

    var eventModel: EventModel? = null

    override var liveLoadindState: MutableLiveData<ProgressStatus> = MutableLiveData(ProgressStatus.FIRST_LOADING)

    val hasNoRequest = ObservableBoolean()

    val hasNoAfter = ObservableBoolean()

    override val listLiveData: LiveData<List<MyEventRequestModel>> = eventRepository.getMyEvent().map { event ->
        com.orhanobut.logger.Logger.e("event is null = ${event == null}")
        if (event == null) {
            hasNoAfter.set(true)
            listOf()
        } else {
            hasNoAfter.set(false)
            hasNoRequest.set(event.userRequest.isEmpty())
            eventModel = event.toEventModel()
            event.userRequest.map { it.toMyEventRequestModel() }
        }


    }

    override fun retryLoading(refresh: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openEvent(myEventRequestModel: MyEventRequestModel, position: Int) {
        liveOpenDetailRequest.changeValue(Pair(position, myEventRequestModel))
    }

    override fun onImageLoaded() {
        liveImageLoaded.call()
    }

    fun seeMyAfter(){
        liveSeeMyAfter.call()
    }

    fun createAfter(){
        liveCreateAfter.call()
    }

    private fun Event.toEventModel(): EventModel {
        return EventModel(uid,
            null,
            picture,
            nbCurrentGuest,
            nbAcceptedGuest,
            DateTime(dateBegin.toDate()),
            DateTime(dateEnd.toDate()),
            description,
            addressOwner,
            phoneOwner)

    }

    private fun UserRequest.toMyEventRequestModel(): MyEventRequestModel {
        return MyEventRequestModel(idUser,
            nameUser,
            phoneOwner,
            pictureUri,
            nbBoy,
            nbGirl,
            nbUndefined,
            description,
            Enums.StateUserRequest.from(stateRequest)
        )
    }

}