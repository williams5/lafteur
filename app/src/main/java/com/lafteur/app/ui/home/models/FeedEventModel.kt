package com.lafteur.app.ui.home.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.PropertyName
import com.lafteur.app.ui.shared.Indexed

data class FeedEventModel(override val id: String,
                          val nameOwner: String,
                          val picture: String,
                          val nbCurrentGuest: Int,
                          val nbGuestAccepted: Int,
                          val dateBegin: Timestamp,
                          val dateEnd: Timestamp,
                          val description: String,
                          val addressOwner: String,
                          val phoneOwner: String) : Indexed {
    constructor() : this("", "", "", 0, 0, Timestamp.now(), Timestamp.now(), "", "", "")
}
