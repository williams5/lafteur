package com.lafteur.app.ui.home.detail

import androidx.databinding.ObservableField
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.call
import com.lafteur.app.helpers.livedata.changeValue
import javax.inject.Inject

class DetailEventViewModel @Inject constructor() : BaseViewModel() {


    val test = ObservableField<String>("helloo word")

    val liveImageLoaded = SingleLiveEvent<Unit>()

    fun onImageLoaded() = liveImageLoaded.call()

}