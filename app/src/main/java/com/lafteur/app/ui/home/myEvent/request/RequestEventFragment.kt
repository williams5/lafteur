package com.lafteur.app.ui.home.myEvent.request

import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.lafteur.app.R
import com.lafteur.app.core.list.BaseListAdapter
import com.lafteur.app.core.list.ListFragment
import com.lafteur.app.core.list.base.ListUIConfig
import com.lafteur.app.databinding.FragmentHomeMyRequestBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.di.DaggerHomeComponent
import kotlinx.android.synthetic.main.fragment_home_my_request.*


class RequestEventFragment : ListFragment<RequestEventModel, RequestEventViewModel, FragmentHomeMyRequestBinding>(), View.OnCreateContextMenuListener {

    override val contentId: Int = R.layout.fragment_home_my_request

    override val listViewModel by lazy { getViewModelFragment<RequestEventViewModel>() }

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override val recyclerView: RecyclerView?
        get() = dataBinding?.rvRequest

    override fun setupDataBinding() {
        super.setupDataBinding()
        dataBinding?.activityVM = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerHomeComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(activity))
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)


        listViewModel.liveOpenDetailEvent.observe(this, Observer { positionFeedEvent ->
            recyclerView?.layoutManager?.findViewByPosition(positionFeedEvent.first)?.let {

            }
        })

        listViewModel.liveSeeAfters.observe(this, Observer {
            activityViewModel.liveOnBottomItemSelected.value = R.id.navigation_home
        })

        listViewModel.liveLongPressEvent.observe(this, Observer {
            rv_request.showContextMenu()
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerForContextMenu(rv_request)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unregisterForContextMenu(rv_request)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity.menuInflater
        inflater.inflate(R.menu.request_menu, menu)

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.open_request -> Toast.makeText(activity, "Open", Toast.LENGTH_SHORT).show()
            R.id.cancel_request -> Toast.makeText(activity, "Delete event", Toast.LENGTH_SHORT).show()
            else  -> return false
        }

        return true
    }


    override fun createUIConfig(): ListUIConfig<RequestEventModel, RequestEventViewModel> =
        object : ListUIConfig<RequestEventModel, RequestEventViewModel>(this) {
            override val layoutId: Int = R.layout.fragment_home_my_request
        }

    override fun createAdapter(): BaseListAdapter<RequestEventModel, RequestEventViewModel, FragmentHomeMyRequestBinding> =
        object : BaseListAdapter<RequestEventModel, RequestEventViewModel, FragmentHomeMyRequestBinding>(this) {
            override fun getCellLayoutId(viewType: Int): Int = R.layout.item_my_request

            override fun createItemViewModel(item: RequestEventModel, index: Int, type: Int): Any {
                return RequestEventItemViewModel(item, index, viewModel)
            }

        }

}