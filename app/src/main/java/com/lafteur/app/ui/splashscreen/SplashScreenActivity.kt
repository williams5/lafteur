/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.ui.splashscreen

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lafteur.app.R
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.databinding.ActivitySplashBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.helpers.extensions.startActivity
import com.lafteur.app.ui.home.HomeActivity
import com.lafteur.app.ui.intro.IntroActivity
import com.lafteur.app.ui.splashscreen.di.DaggerSplashComponent
import kotlinx.android.synthetic.main.activity_splash.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashScreenActivity : BaseActivity<SplashScreenViewModel,ActivitySplashBinding>(SplashScreenViewModel::class, R.layout.activity_splash) {

    private var mShouldFinishAfterTransition = false

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerSplashComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        super.onCreate(savedInstanceState)

        viewModel.onResponseUserLogged().observe(this, Observer { userIsLogged ->
            when (userIsLogged) {
                true -> {
                    startActivity<HomeActivity>()
                }
                false -> {
                    startActivity<IntroActivity>()
                }
            }
        })
        Get_hash_key()
        /*
        activity_splash_logo
            .animate()
            .alpha(1f)
            .setDuration(1000)
            .setStartDelay(500)
            .withEndAction {
                activity_splash_title
                    .animate()
                    .alpha(1f)
                    .setDuration(800)
                    .withEndAction {
                        viewModel.onResponseUserLogged().observe(this, Observer { userIsLogged ->

                            activity_splash_title
                                .animate()
                                .alpha(0f)
                                .setDuration(800)
                                .setStartDelay(1000)
                                .withEndAction {
                                    when (userIsLogged) {
                                        true -> {
                                            startActivity<HomeActivity>()
                                        }
                                        false -> {
                                            startActivity<IntroActivity>()
                                        }
                                    }
                                }
                                .start()


                            mShouldFinishAfterTransition = true
                        })
                    }
                    .start()
            }.start()

         */


    }


    fun Get_hash_key() {
        val info: PackageInfo
        try {
            info = packageManager.getPackageInfo("com.lafteur.app", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }

    }

    override fun onStop() {
        super.onStop()
        if (mShouldFinishAfterTransition) {
            finish()
        }
    }

}