package com.lafteur.app.ui.home.myEvent

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentHomeMyEventBinding
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.myEvent.event.MyEventRequestFragment
import com.lafteur.app.ui.home.myEvent.request.RequestEventFragment

class HomeMyEventFragment : BaseFragment<FragmentHomeMyEventBinding>() {

    override val contentId: Int = R.layout.fragment_home_my_event

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
        dataBinding?.vpMyEvent?.apply {
            adapter = object : FragmentStateAdapter(parentFragmentManager, activity.lifecycle) {
                val listFragment = listOf(RequestEventFragment(), MyEventRequestFragment())

                override fun getItemCount(): Int {
                    return listFragment.size
                }

                override fun createFragment(position: Int): Fragment {
                    return listFragment[position]
                }
            }

            isSaveEnabled = false
            isUserInputEnabled = false
            dataBinding?.tabLayout?.let {
                TabLayoutMediator(it, this) { tab, position ->
                    when (position) {
                        0 -> tab.setText(R.string.request)
                        else -> tab.setText(R.string.my_after)
                    }
                }.attach()
            }

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


}