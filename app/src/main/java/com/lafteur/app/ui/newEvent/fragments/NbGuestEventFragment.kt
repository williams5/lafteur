package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventNbGuestBinding
import com.lafteur.app.ui.newEvent.NewEventViewModel
import com.lafteur.app.ui.shared.DefaultNmberPickerListener
import kotlinx.android.synthetic.main.fragment_new_event_nb_guest.*
import kotlin.reflect.KClass

class NbGuestEventFragment : BaseFragment<FragmentNewEventNbGuestBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_nb_guest

    var total = 0

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_event_nb_guest, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityViewModel.eventModel.nbCurrentGuest?.let {
            dnpv_current_guest.setInitValue(it)
        }

        activityViewModel.eventModel.nbGuestAccepted?.let {
            dnpv_accepted_guest.setInitValue(it)
        }


        updateButtonTotal()

        dnpv_accepted_guest.listener = object : DefaultNmberPickerListener {
            override fun onValueChange(oldValue: Int, value: Int) {
                activityViewModel.updateEventModel(nbGuestAccepted = value)
                updateButtonTotal()
            }
        }

        dnpv_current_guest.listener = object : DefaultNmberPickerListener {
            override fun onValueChange(oldValue: Int, value: Int) {
                activityViewModel.updateEventModel(nbCurrentGuest = value)
                updateButtonTotal()
            }
        }

        btn_next.setOnClickListener {
            if(activityViewModel.isUpdateMode){
                findNavController().navigate(R.id.go_to_updateEventFragment)
            }
            else {
                findNavController().navigate(R.id.go_to_dateBeginEventFragment)
            }
        }


    }

    fun updateButtonTotal() {
        val total = dnpv_accepted_guest.currentValue + dnpv_current_guest.currentValue
        btn_next.isEnabled = total > 0
        btn_next.text = resources.getQuantityString(R.plurals.total_guest_new_event, total, total)
    }

}