package com.lafteur.app.ui.home.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.list.PagedListViewModel
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.helpers.firestore.FirestorePagedDataSourceImpl
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.call
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.infrastructure.event.Event
import com.lafteur.app.infrastructure.event.EventQueryFactory
import com.lafteur.app.ui.home.models.FeedEventModel
import com.orhanobut.logger.Logger
import javax.inject.Inject


class FeedViewModel @Inject constructor(eventQueryFactory: EventQueryFactory) : PagedListViewModel<FeedEventModel>(), FeedItemActionListener {


    private val firestorePagedDataSource = FirestorePagedDataSourceImpl.create<Event>(eventQueryFactory.queryPagedForType(Enums.EventType.ALL_EVENT, limit = 5))

    val liveOpenDetailEvent = SingleLiveEvent<Pair<Int, FeedEventModel>>()

    private val liveImageLoaded = SingleLiveEvent<Unit>()

    override var liveLoadindState: MutableLiveData<ProgressStatus> = firestorePagedDataSource.liveLoadindState

    override val listLiveData: LiveData<List<FeedEventModel>> = firestorePagedDataSource.listLive.map { it.map { event -> event.toFeedEventModel() } }

    override val hasNextPage: LiveData<Boolean> = firestorePagedDataSource.hasNextPage

    init {
        Logger.d("Init feedviewmodel")
        firestorePagedDataSource.loadInitial()
    }

    override fun loadNextData() {
        Logger.d("load next page")
        firestorePagedDataSource.loadNext()
    }

    override fun retryLoading(refresh: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onImageLoaded() {
        liveImageLoaded.call()
    }

    override fun openEvent(event: FeedEventModel, position: Int) {
        liveOpenDetailEvent.changeValue(Pair(position, event))
    }

    override fun onCleared() {
        firestorePagedDataSource.clear()
        super.onCleared()
    }


    private fun Event.toFeedEventModel(): FeedEventModel {
        return FeedEventModel(uid,
            nameOwner,
            picture,
            nbCurrentGuest,
            nbAcceptedGuest,
            dateBegin,
            dateEnd,
            description,
            addressOwner,
            phoneOwner)
    }
}