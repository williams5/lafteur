package com.lafteur.app.ui.joinEvent.di

import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.di.ViewModelModule
import com.lafteur.app.ui.joinEvent.JoinEventActivity
import com.lafteur.app.ui.newEvent.NewEventActivity
import dagger.Component

@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface JoinEventComponent {
    fun inject(joinEventActivity: JoinEventActivity)
}