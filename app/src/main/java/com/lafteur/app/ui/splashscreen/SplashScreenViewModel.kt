package com.lafteur.app.ui.splashscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.infrastructure.user.UserRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(val userRepository: UserRepository) : BaseViewModel() {

    private var liveUserLogged = MutableLiveData<Boolean>()

    fun onResponseUserLogged(): LiveData<Boolean> {
        return liveUserLogged
    }


    init {
        userRepository.isCurrentUserLogged()
            .subscribeBy(
                onSuccess = { isLogged ->
                    Observable.timer(0, TimeUnit.SECONDS)
                        .subscribe {
                            liveUserLogged.postValue(isLogged)
                        }.addTo(compositeDisposable)
                }
            ).addTo(compositeDisposable)

    }


}