package com.lafteur.app.ui.authentification.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentAuthBinding
import com.lafteur.app.ui.authentification.AuthViewModel
import com.lafteur.app.ui.home.HomeViewModel
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlin.reflect.KClass


class AuthFragment : BaseFragment<FragmentAuthBinding>() {

    override val contentId: Int = R.layout.fragment_auth

    private val activityViewModel: AuthViewModel by lazy { getViewModelActivity<AuthViewModel>() }

    private lateinit var callBackManager: CallbackManager

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callBackManager = CallbackManager.Factory.create()


        LoginManager.getInstance().registerCallback(callBackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                activity.showProgressDialog(true)
                activityViewModel.handleFacebookLogin(result!!)
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {

            }

        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        et_phone.setOnClickListener {
            goToFragmentWithAnimation(R.id.go_to_phoneFragment,et_phone to et_phone.transitionName, ccp to ccp.transitionName)
            activity.supportActionBar?.show()
        }
        btn_fb.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile",
                "email",
                "user_events",
                "user_birthday",
                "user_location",
                "user_likes",
                "user_gender",
                "user_friends"))
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callBackManager.onActivityResult(requestCode, resultCode, data)
    }





}