package com.lafteur.app.ui.joinEvent.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentJoinEventUpdateBinding
import com.lafteur.app.helpers.extensions.launchCropImage
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_join_event_update.*

class UpdateJoinEventFragment : BaseFragment<FragmentJoinEventUpdateBinding>() {

    override val contentId: Int = R.layout.fragment_join_event_update

    private val activityViewModel by lazy { getViewModelActivity<JoinEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onResume() {
        super.onResume()
        activityViewModel.isUpdateMode = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cuv_picture.setOnClickListener {
            launchCropImage()
        }

        cuv_description.setOnClickListener {
            goTo(R.id.go_to_descriptionJoinEventFragment)
        }

        cuv_nb_guest.setOnClickListener {
            goTo(R.id.go_to_nbGuestJoinEventFragment)
        }

        cuv_infos.setOnClickListener {
            goTo(R.id.go_to_phoneOwnerJoinEventFragment)
        }

    }

    private fun goTo(actionId: Int) {
        activityViewModel.isUpdateMode = true
        findNavController().navigate(actionId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val result = CropImage.getActivityResult(data)
                        activityViewModel.updateJoinEventModel(pictureUri = result.uri.toString())
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {

                    }
                    else -> {

                    }
                }
            }
        }
    }
}