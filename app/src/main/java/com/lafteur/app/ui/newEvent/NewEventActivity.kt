package com.lafteur.app.ui.newEvent

import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import androidx.constraintlayout.widget.ConstraintSet
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.lafteur.app.R
import com.lafteur.app.constants.Constants
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.databinding.ActivityNewEventBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.ui.newEvent.di.DaggerNewEventComponent
import com.lafteur.app.ui.newEvent.models.EventModel
import kotlinx.android.synthetic.main.activity_new_event.*


class NewEventActivity : BaseActivity<NewEventViewModel, ActivityNewEventBinding>(NewEventViewModel::class, R.layout.activity_new_event) {


    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerNewEventComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        super.onCreate(savedInstanceState)

        if (intent.hasExtra(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT)) {
            val pictureUri = intent.getStringExtra(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT)
            viewModel.updateEventModel(pictureUri = pictureUri)

        } else {
            val eventModel = intent.getSerializableExtra(Constants.BUNDLE_EVENT_MODEL) as EventModel
            viewModel.updateEventModel(eventModel)
            viewModel.isUpdateMode = true
            val navOptions = NavOptions.Builder()
                .setPopUpTo(R.id.updateEventFragment, true)
                .build()
            findNavController(R.id.nav_host_new_event_fragment).navigate(R.id.go_to_resumeEventFragment,null,navOptions)
        }


    }

    fun fullscreenFrag(fullscreen: Boolean) {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cl_new_event)
        constraintSet.clear(nav_host_new_event_fragment.id, ConstraintSet.TOP)
        if (fullscreen) {
            constraintSet.connect(nav_host_new_event_fragment.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
        } else {
            constraintSet.connect(nav_host_new_event_fragment.id, ConstraintSet.TOP, toolbar.id, ConstraintSet.BOTTOM)
        }

        val transition = ChangeBounds()
        transition.duration = 300
        TransitionManager.beginDelayedTransition(cl_new_event, transition)
        constraintSet.applyTo(cl_new_event)
    }

}
