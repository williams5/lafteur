package com.lafteur.app.ui.splashscreen.di

import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.di.ViewModelModule
import com.lafteur.app.ui.splashscreen.SplashScreenActivity
import dagger.Component

@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface SplashComponent {
    fun inject(splashScreenActivity: SplashScreenActivity)
}