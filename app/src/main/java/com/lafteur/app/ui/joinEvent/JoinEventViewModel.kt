package com.lafteur.app.ui.joinEvent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.infrastructure.event.EventRepository
import com.lafteur.app.infrastructure.event.UserRequest
import com.lafteur.app.infrastructure.upload.UploadRepository
import com.lafteur.app.infrastructure.user.User
import com.lafteur.app.infrastructure.user.UserRepository
import com.lafteur.app.ui.joinEvent.models.JoinEventModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class JoinEventViewModel @Inject constructor(private val userRepository: UserRepository,
                                             private val eventRepository: EventRepository,
                                             private val uploadRepository: UploadRepository) : BaseViewModel() {

    private val firebaseAuth = FirebaseAuth.getInstance()

    var isUpdateMode = false

    val joinEventModel = JoinEventModel()

    var descriptionEvent = ""

    private val liveEventJoined = MutableLiveData<Boolean>()

    fun onUserLoaded(): LiveData<User> {
        return userRepository.getLiveUser(firebaseAuth.currentUser!!.uid)
    }

    fun onEventJoined(): LiveData<Boolean> {
        return liveEventJoined
    }

    fun joinEvent() {

        viewModelScope.launch {
            try {
                eventRepository.jointEvent(joinEventModel.idEvent, joinEventModel.toUserRequest())
                liveEventJoined.changeValue(true)
            }
            catch (e:Exception){
                liveEventJoined.changeValue(false)
            }

        }
/*
        uploadRepository.uploadEventImage(eventModel.pictureUri!!)
            .flatMapCompletable {
                eventModel.pictureUri = it
                eventRepository.createEvent(eventModel.toEvent())
            }.subscribeBy(
                onComplete = {
                    liveEventCreated.postValue(true)
                },
                onError = {
                    liveEventCreated.postValue(false)
                }
            ).addTo(compositeDisposable)


 */
    }


    fun updateJoinEventModel(idEvent: String? = null,
                             owner: User? = null,
                             pictureUri: String? = null,
                             nbBoy: Int? = null,
                             nbGirl: Int? = null,
                             nbUndefined: Int? = null,
                             description: String? = null,
                             phoneOwner: String? = null) {
        joinEventModel.idEvent = idEvent ?: joinEventModel.idEvent
        joinEventModel.owner = owner ?: joinEventModel.owner
        joinEventModel.pictureUri = pictureUri ?: joinEventModel.pictureUri
        joinEventModel.nbBoy = nbBoy ?: joinEventModel.nbBoy
        joinEventModel.nbGirl = nbGirl ?: joinEventModel.nbGirl
        joinEventModel.nbUndefined = nbUndefined ?: joinEventModel.nbUndefined
        joinEventModel.description = description ?: joinEventModel.description
        joinEventModel.phoneOwner = phoneOwner ?: joinEventModel.phoneOwner

    }

    private fun JoinEventModel.toUserRequest(): UserRequest {
        return UserRequest(owner!!.uid, owner!!.name, owner!!.phone, pictureUri!!, nbBoy!!, nbGirl!!, nbUndefined!!, description!!, Enums.StateUserRequest.WAITING.value)

    }
}