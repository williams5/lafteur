package com.lafteur.app.ui.shared

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.lafteur.app.R
import com.lafteur.app.helpers.extensions.afterTextChanged
import com.lafteur.app.helpers.extensions.dissmissKeyboard
import com.lafteur.app.helpers.extensions.isValidEmail
import kotlinx.android.synthetic.main.view_edit_text_default.view.*

class DefaultEditText(context: Context, attrs: AttributeSet? = null) : FrameLayout(context, attrs) {

    private var errorMessage = context.getString(R.string.error_msg_edit_field_required_default)

    var text = ""
        get() = edit_field.text.toString()
        set(value) {
            field = value
            edit_field.setText(field)
            placeCursorAtEnd()
        }

    var hint = ""
        set(value) {
            field = value
            til_field.hint = field
        }

    private var isRequired = false
    private var minLength: Int = -1
    private var maxLength: Int = -1

    var listener: DefaultEditTextListener? = null

    init {

        LayoutInflater.from(context).inflate(R.layout.view_edit_text_default, this, true)

        edit_field.id = View.generateViewId()

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.DefaultEditText, 0, 0)

            typedArray.getString(R.styleable.DefaultEditText_value)?.let {
                text = it
                edit_field.setText(text)
            }
            typedArray.getString(R.styleable.DefaultEditText_android_hint)?.let {
                hint = it
            }
            typedArray.getDimensionPixelSize(
                R.styleable.DefaultEditText_android_textSize,
                edit_field.textSize.toInt()
            ).let {
                //edit_field.setTextSize(TypedValue.COMPLEX_UNIT_PX, it.toFloat())
            }
            typedArray.getInt(R.styleable.DefaultEditText_android_inputType, InputType.TYPE_CLASS_TEXT).let {
                edit_field.inputType = it
            }

            typedArray.getString(R.styleable.DefaultEditText_error_message)?.let {
                errorMessage = it
            }
            typedArray.getInt(R.styleable.DefaultEditText_android_minEms, 0).let {
                if (it > 0) {
                    edit_field.minEms = it
                }
            }
            typedArray.getBoolean(R.styleable.DefaultEditText_android_focusable, true).let {
                edit_field.isFocusable = it
            }

            isRequired = typedArray.getBoolean(R.styleable.DefaultEditText_isRequired, false)
            val minDefaultValue = if (isRequired) {
                1
            } else {
                -1
            }
            minLength = typedArray.getInt(R.styleable.DefaultEditText_minLength, minDefaultValue)
            maxLength = typedArray.getInt(R.styleable.DefaultEditText_maxLength, -1)

            typedArray.recycle()
        }


        edit_field.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                validateRequiredField()
            }
        }

        edit_field.setOnEditorActionListener { textView, actionId, _ ->
            //Hide keyboar on done button and valid field
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateRequiredField()
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(textView.windowToken, 0)
                listener?.onImeActionDone()

                true
            } else {
                false
            }
        }

        edit_field.afterTextChanged {
            if (it.isNotEmpty()) {
                setIsValidInput()
                til_field.isErrorEnabled = false
            } else {
                setNoneInput()
            }
            listener?.afterTextChanged(it)

        }

        edit_field.setOnClickListener {
            performClick()
        }


    }

    fun placeCursorAtEnd() {
        edit_field.setSelection(text.length)
    }

    fun placeCursorAt(position:Int){
        edit_field.setSelection(position)
    }

    fun dissmissKeyboard() {
        edit_field.dissmissKeyboard()
    }

    fun clear() {
        text = ""
    }

    fun validateRequiredField() {
        when (edit_field?.inputType) {
            InputType.TYPE_CLASS_TEXT -> {

            }
        }

        if (isRequired && !isValidField()) {
            til_field.isErrorEnabled = true
            til_field.error = errorMessage
            setIsErrorInput()
        } else {
            setIsValidInput()
        }
    }

    private fun setNoneInput() {
        edit_field.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
    }


    private fun setIsValidInput() {
        val drawableInputError = context.resources.getDrawable(R.drawable.ic_check, context.theme)
        edit_field.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableInputError, null)
    }

    private fun setIsErrorInput() {
        val drawableInputError = context.resources.getDrawable(R.drawable.ic_clear, context.theme)
        edit_field.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableInputError, null)
    }

    fun isValid(): Boolean {
        return isValidField()
    }

    fun putCursorToTheEnd() {
        edit_field.setSelection(text.length)
    }

    private fun isValidField(): Boolean {
        if (!isRequired) {
            return true
        }

        //email validation
        if (edit_field.inputType == (InputType.TYPE_CLASS_TEXT + InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)) {
            return text.isValidEmail()
        }

        //default text validation with min and maxLength
        if (minLength > -1 && text.length < minLength) {
            return false
        }

        if (maxLength > -1 && text.length > maxLength) {
            return false
        }

        return true
    }

    interface DefaultEditTextListener {
        fun onImeActionDone()
        fun afterTextChanged(text: String)
    }
}
