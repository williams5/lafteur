package com.lafteur.app.ui.home.myEvent.request

import com.google.firebase.Timestamp
import com.lafteur.app.constants.Enums
import com.lafteur.app.ui.shared.Indexed

data class RequestEventModel(override val id: String,
                             val nameOwner: String,
                             val picture: String,
                             val nbCurrentGuest: Int,
                             val nbGuestAccepted: Int,
                             val dateBegin: Timestamp,
                             val dateEnd: Timestamp,
                             val description: String,
                             val addressOwner: String,
                             val phoneOwner: String,
                             val stateUserRequest: Enums.StateUserRequest) : Indexed {
    constructor() : this("", "", "", 0, 0, Timestamp.now(), Timestamp.now(), "", "", "", Enums.StateUserRequest.WAITING)
}



