package com.lafteur.app.ui.joinEvent.models

import com.lafteur.app.infrastructure.user.User

data class JoinEventModel(var owner: User?,
                          var phoneOwner: String?,
                          var pictureUri: String?,
                          var nbBoy: Int?,
                          var nbGirl: Int?,
                          var nbUndefined: Int?,
                          var description: String?,
                          var idEvent: String) {
    constructor() : this(null,
        null,
        null,
        2,
        2,
        2,
        "COUCOUU LES AMIS","")
}
