package com.lafteur.app.ui.home

import android.os.Bundle
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lafteur.app.R
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.databinding.ActivityHomeBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.ui.home.di.DaggerHomeComponent
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomeViewModel, ActivityHomeBinding>(HomeViewModel::class, R.layout.activity_home){

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerHomeComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        super.onCreate(savedInstanceState)
    }

}
