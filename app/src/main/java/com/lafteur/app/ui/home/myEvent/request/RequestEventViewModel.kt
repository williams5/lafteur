package com.lafteur.app.ui.home.myEvent.request

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.list.PagedListViewModel
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.helpers.firestore.FirestorePagedDataSourceImpl
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.call
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.infrastructure.event.Event
import com.lafteur.app.infrastructure.event.EventQueryFactory
import com.lafteur.app.infrastructure.event.EventRepository
import com.orhanobut.logger.Logger
import javax.inject.Inject

class RequestEventViewModel @Inject constructor(eventQueryFactory: EventQueryFactory) : PagedListViewModel<RequestEventModel>(), RequestEventItemActionListener {


    val firestorePagedDataSource = FirestorePagedDataSourceImpl.create<Event>(eventQueryFactory.queryPagedForType(Enums.EventType.REQUEST_EVENT, limit = 5))

    val currentUser = FirebaseAuth.getInstance().currentUser

    val liveOpenDetailEvent = SingleLiveEvent<Pair<Int, RequestEventModel>>()

    val liveLongPressEvent = SingleLiveEvent<Pair<Int, RequestEventModel>>()

    val liveImageLoaded = SingleLiveEvent<Unit>()

    val liveSeeAfters = SingleLiveEvent<Unit>()

    override var liveLoadindState: MutableLiveData<ProgressStatus> = firestorePagedDataSource.liveLoadindState

    override val listLiveData: LiveData<List<RequestEventModel>> = firestorePagedDataSource.listLive.map { it.map { event -> event.toRequestEventModel(currentUser) } }

    override val hasNextPage: LiveData<Boolean> = firestorePagedDataSource.hasNextPage

    init {
        Logger.d("Init feedviewmodel")
        firestorePagedDataSource.loadInitial()
    }

    override fun loadNextData() {
        Logger.d("load next page")
        firestorePagedDataSource.loadNext()
    }

    override fun retryLoading(refresh: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onImageLoaded() {
        liveImageLoaded.call()
    }

    override fun openEvent(requestEventModel: RequestEventModel, position: Int) {
        liveOpenDetailEvent.changeValue(Pair(position, requestEventModel))
    }

    override fun longPressEvent(requestEventModel: RequestEventModel, position: Int) {
        liveLongPressEvent.changeValue(Pair(position, requestEventModel))
    }

    override fun onCleared() {
        firestorePagedDataSource.clear()
        super.onCleared()
    }

    fun seeAfters(){
        liveSeeAfters.call()
    }

    fun cancelRequest() {

    }


    private fun Event.toRequestEventModel(currentUser: FirebaseUser?): RequestEventModel {
        return RequestEventModel(uid,
            nameOwner,
            picture,
            nbCurrentGuest,
            nbAcceptedGuest,
            dateBegin,
            dateEnd,
            description,
            addressOwner,
            phoneOwner,
            Enums.StateUserRequest.from(userRequest.first { it.idUser == currentUser?.uid }.stateRequest)
        )
    }

}