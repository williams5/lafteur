package com.lafteur.app.ui.newEvent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.Timestamp
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.call
import com.lafteur.app.infrastructure.event.Event
import com.lafteur.app.infrastructure.event.EventRepository
import com.lafteur.app.infrastructure.upload.UploadRepository
import com.lafteur.app.infrastructure.user.User
import com.lafteur.app.infrastructure.user.UserRepository
import com.lafteur.app.ui.newEvent.models.EventModel
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import javax.inject.Inject

class NewEventViewModel @Inject constructor(private val userRepository: UserRepository,
                                            private val eventRepository: EventRepository,
                                            private val uploadRepository: UploadRepository) : BaseViewModel() {

    var isUpdateMode = false

    val eventModel = EventModel()

    private val liveEventCreated = MutableLiveData<Boolean>()
    val liveViewModelInitialized = SingleLiveEvent<Unit>()

    init {

        viewModelScope.launch {
            val user = userRepository.getMyInfos()
            com.orhanobut.logger.Logger.d(user)
            val phone = if (user.phone.isEmpty()) {
                null
            } else {
                user.phone
            }
            updateEventModel(owner = user, phoneOwner = phone)
           // liveViewModelInitialized.call()
        }

    }

    fun onEventCreated(): LiveData<Boolean> {
        return liveEventCreated
    }

    fun createEvent() {

        uploadRepository.uploadEventImage(eventModel.pictureUri!!)
            .flatMapCompletable {
                eventModel.pictureUri = it
                eventRepository.createEvent(eventModel.toEvent())
            }.subscribeBy(
                onComplete = {
                    liveEventCreated.postValue(true)
                },
                onError = {
                    liveEventCreated.postValue(false)
                }
            ).addTo(compositeDisposable)

    }


    fun updateEventModel(idEvent: String? = null,
                         owner: User? = null,
                         pictureUri: String? = null,
                         nbCurrentGuest: Int? = null,
                         nbGuestAccepted: Int? = null,
                         dateBegin: DateTime? = null,
                         dateEnd: DateTime? = null,
                         description: String? = null,
                         addressEvent: String? = null,
                         phoneOwner: String? = null) {
        eventModel.idEvent = idEvent ?: eventModel.idEvent
        eventModel.owner = owner ?: eventModel.owner
        eventModel.pictureUri = pictureUri ?: eventModel.pictureUri
        eventModel.nbCurrentGuest = nbCurrentGuest ?: eventModel.nbCurrentGuest
        eventModel.nbGuestAccepted = nbGuestAccepted ?: eventModel.nbGuestAccepted
        eventModel.dateBegin = dateBegin ?: eventModel.dateBegin
        eventModel.dateEnd = dateEnd ?: eventModel.dateEnd
        eventModel.description = description ?: eventModel.description
        eventModel.adressEvent = addressEvent ?: eventModel.adressEvent
        eventModel.phoneOwner = phoneOwner ?: eventModel.phoneOwner

    }

    fun updateEventModel(event: EventModel) {
        updateEventModel(event.idEvent,
            event.owner,
            event.pictureUri,
            event.nbCurrentGuest,
            event.nbGuestAccepted,
            event.dateBegin,
            event.dateEnd,
            event.description,
            event.adressEvent,
            event.phoneOwner)
    }

    private fun EventModel.toEvent(): Event {

        return Event(adressEvent!!,
            Timestamp(dateBegin!!.toDate()),
            Timestamp(dateEnd!!.toDate()),
            description!!,
            owner!!.name,
            nbGuestAccepted!!,
            nbCurrentGuest!!,
            phoneOwner!!,
            pictureUri!!,
            owner!!.uid)
    }
}