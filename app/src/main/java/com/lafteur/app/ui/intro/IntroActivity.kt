package com.lafteur.app.ui.intro

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.lafteur.app.R
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.helpers.extensions.Half
import com.lafteur.app.helpers.extensions.setupWithViewPager
import com.lafteur.app.helpers.extensions.startActivity
import com.lafteur.app.ui.authentification.AuthActivity
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        val adapter = IntroAdapter(supportFragmentManager)
        vp_intro.adapter = adapter
        ci_intro.setViewPager(vp_intro)
        lav_intro.setupWithViewPager(vp_intro, Half)

        btn_connect.setOnClickListener {
            startActivity<AuthActivity>()
        }
    }
}