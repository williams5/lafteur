package com.lafteur.app.ui.joinEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentJoinEventDescriptionBinding
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import kotlinx.android.synthetic.main.fragment_join_event_description.*

class DescriptionJoinEventFragment : BaseFragment<FragmentJoinEventDescriptionBinding>() {

    override val contentId: Int = R.layout.fragment_join_event_description

    private val activityViewModel by lazy { getViewModelActivity<JoinEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btn_next.setOnClickListener {
            activityViewModel.updateJoinEventModel(description = et_description.text.toString())
            if (activityViewModel.isUpdateMode) {
                findNavController().navigate(R.id.go_to_updateJoinEventFragment)
            } else {
                findNavController().navigate(R.id.go_to_phoneOwnerJoinEventFragment)
            }

        }
    }

}