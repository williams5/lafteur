package com.lafteur.app.ui.newEvent.di

import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.di.ViewModelModule
import com.lafteur.app.ui.newEvent.NewEventActivity
import dagger.Component

@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface NewEventComponent {
    fun inject(newEventActivity: NewEventActivity)
}