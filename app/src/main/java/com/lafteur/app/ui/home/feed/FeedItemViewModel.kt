package com.lafteur.app.ui.home.feed

import com.lafteur.app.ui.home.models.FeedEventModel

interface FeedItemActionListener {
    fun openEvent(event: FeedEventModel, position: Int)
    fun onImageLoaded()
}

class FeedItemViewModel(val event: FeedEventModel, private val position: Int, private val actionListener: FeedItemActionListener) {

    fun onImageLoaded() {
        if (position == 1)
            actionListener.onImageLoaded()
    }

    fun openEvent() {
        actionListener.openEvent(event, position)
    }

}