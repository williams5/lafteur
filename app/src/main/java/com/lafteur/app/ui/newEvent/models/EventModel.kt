package com.lafteur.app.ui.newEvent.models

import com.lafteur.app.infrastructure.user.User
import org.joda.time.DateTime
import java.io.Serializable

data class EventModel(var idEvent: String?,
                      var owner: User?,
                      var pictureUri: String?,
                      var nbCurrentGuest: Int?,
                      var nbGuestAccepted: Int?,
                      var dateBegin: DateTime?,
                      var dateEnd: DateTime?,
                      var description: String?,
                      var adressEvent: String?,
                      var phoneOwner: String?): Serializable {
    constructor() : this(null,
        null,
        null,
        0,
        0,
        null,
        null,
        null,
        null,
        null)


}
