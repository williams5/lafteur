package com.lafteur.app.ui.authentification.di

import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.di.ViewModelModule
import com.lafteur.app.ui.authentification.AuthActivity
import dagger.Component

@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface AuthComponent {
    fun inject(authActivity: AuthActivity)
}