package com.lafteur.app.ui.newEvent.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventUpdateBinding
import com.lafteur.app.helpers.extensions.launchCropImage
import com.lafteur.app.ui.newEvent.NewEventViewModel
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_auth_phone.*
import kotlinx.android.synthetic.main.fragment_new_event_update.*
import kotlin.reflect.KClass

class UpdateEventFragment : BaseFragment<FragmentNewEventUpdateBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_update

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cuv_picture.setOnClickListener {
            launchCropImage()
        }

        cuv_date.setOnClickListener {
            goTo(R.id.go_to_dateBeginEventFragment)
        }

        cuv_description.setOnClickListener {
            goTo(R.id.go_to_descriptionEventFragment)
        }

        cuv_nb_guest.setOnClickListener {
            goTo(R.id.go_to_nbGuestEventFragment)
        }

        cuv_infos.setOnClickListener {
            goTo(R.id.go_to_infosOwnerEventFragment)
        }

    }

    private fun goTo(actionId: Int) {
        activityViewModel.isUpdateMode = true
        findNavController().navigate(actionId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val result = CropImage.getActivityResult(data)
                        activityViewModel.updateEventModel(pictureUri = result.uri.toString())
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {

                    }
                    else -> {

                    }
                }
            }
        }
    }
}