package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventDescriptionBinding
import com.lafteur.app.ui.newEvent.NewEventViewModel
import kotlinx.android.synthetic.main.fragment_new_event_description.*
import kotlin.reflect.KClass

class DescriptionEventFragment : BaseFragment<FragmentNewEventDescriptionBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_description

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next.setOnClickListener {
            activityViewModel.updateEventModel(description = et_description.text.toString())
            if (activityViewModel.isUpdateMode) {
                findNavController().navigate(R.id.go_to_updateEventFragment)
            } else {
                findNavController().navigate(R.id.go_to_infosOwnerEventFragment)
            }

        }

    }
}