package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventRecapDateBinding
import com.lafteur.app.ui.newEvent.NewEventViewModel
import kotlinx.android.synthetic.main.fragment_auth_phone.*
import kotlinx.android.synthetic.main.fragment_new_event_recap_date.*
import kotlin.reflect.KClass

class ResumeDateEventFragment : BaseFragment<FragmentNewEventRecapDateBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_recap_date

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_date_begin.text = activityViewModel.eventModel.dateBegin!!.toString("d MMMM")
        tv_hour_begin.text = activityViewModel.eventModel.dateBegin!!.toString("HH:mm")
        tv_date_end.text = activityViewModel.eventModel.dateEnd!!.toString("d MMMM")
        tv_hours_end.text = activityViewModel.eventModel.dateEnd!!.toString("HH:mm")

        btn_save.setOnClickListener {
            if (activityViewModel.isUpdateMode) {
                findNavController().navigate(R.id.go_to_resumeEventFragment)
            } else {
                findNavController().navigate(R.id.go_to_descriptionEventFragment)
            }

        }

    }

}