package com.lafteur.app.ui.shared

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.lafteur.app.R
import kotlinx.android.synthetic.main.view_default_number_picker.view.*

class DefaultNmberPickerPickerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr), DefaultNmberPickerListener {

     var listener: DefaultNmberPickerListener? = null
    var maxCanBeSelected: Int = 200
    var currentQuantityPictureSelection: Int = 0
    var currentValue: Int = 0
        private set

    init {
        LayoutInflater.from(context).inflate(R.layout.view_default_number_picker, this, true)
        orientation = HORIZONTAL

        btn_more.setOnClickListener {
            increment()
        }
        btn_less.setOnClickListener {
            decrement()
        }
    }

    fun setInitValue(value: Int) {
        this.currentValue = value
        onValueChange(0,value)
    }

    private fun increment() {
        currentValue++
        onValueChange(currentValue - 1, currentValue)
    }

    private fun decrement() {
        if (currentValue > 0) {
            currentValue--
            onValueChange(currentValue + 1, currentValue)
        }
    }

    override fun onValueChange(oldValue: Int, value: Int) {
        if(value > 0){
            btn_less.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white))
        }
        else {
            btn_less.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greyish_brown))
        }

        listener?.onValueChange(oldValue, currentValue)

        if (currentQuantityPictureSelection < maxCanBeSelected) {
            tv_number.text = value.toString()
        } else {
            currentValue = oldValue
        }
    }
}

interface DefaultNmberPickerListener {
    fun onValueChange(oldValue: Int, value: Int)
}
