package com.lafteur.app.ui.home

import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import com.lafteur.app.helpers.livedata.changeValue
import com.lafteur.app.ui.home.models.FeedEventModel
import javax.inject.Inject

class HomeViewModel @Inject constructor() : BaseViewModel() {

    val liveOnBottomItemSelected = SingleLiveEvent<@IdRes Int>()

    val selectedEvent = ObservableField<FeedEventModel>()

    val currentPage = ObservableInt(0)

    fun onBottomItemSelected(item: MenuItem): Boolean  {
        liveOnBottomItemSelected.changeValue(item.itemId)
        return true
    }

    fun onPageChanged(position: Int) = currentPage.set(position)
}