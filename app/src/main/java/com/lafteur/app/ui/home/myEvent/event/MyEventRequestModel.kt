package com.lafteur.app.ui.home.myEvent.event

import com.lafteur.app.constants.Enums
import com.lafteur.app.ui.shared.Indexed

data class MyEventRequestModel(override val id: String,
                               val nameUser: String,
                               val phoneUser: String,
                               val pictureUrl: String,
                               val nbBoy: Int,
                               val nbGirl: Int,
                               val nbUndefined: Int,
                               val description: String,
                               val stateUserRequest: Enums.StateUserRequest) : Indexed {
    constructor() : this("", "", "", "", 0, 0, 0, "", Enums.StateUserRequest.WAITING)
}


