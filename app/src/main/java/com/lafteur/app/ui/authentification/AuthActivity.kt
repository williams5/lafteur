package com.lafteur.app.ui.authentification

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.constants.Enums
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.databinding.ActivityAuthBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.helpers.extensions.getCurrentNavigationFragment
import com.lafteur.app.helpers.extensions.startActivity
import com.lafteur.app.ui.authentification.di.DaggerAuthComponent
import com.lafteur.app.ui.authentification.fragment.AuthFragment
import com.lafteur.app.ui.authentification.fragment.VerifPhoneFragment
import com.lafteur.app.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : BaseActivity<AuthViewModel, ActivityAuthBinding>(AuthViewModel::class, R.layout.activity_auth) {



    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAuthComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        viewModel.onStatusLoginChanged().observe(this, statusLoginObserver)
    }

    private val statusLoginObserver = Observer<Enums.SatusLogin> { statusLogin ->
        hideProgressDialog()
        when (statusLogin) {
            Enums.SatusLogin.FB_LOGIN_SUCCESS -> {
                startActivity<HomeActivity>()
            }
            Enums.SatusLogin.PHONE_LOGIN_SUCCESS -> {
                startActivity<HomeActivity>()
            }
            Enums.SatusLogin.NOT_REGISTERED -> {
                val currentFragment = supportFragmentManager.getCurrentNavigationFragment()
                currentFragment?.let {
                    if (currentFragment is VerifPhoneFragment) {
                        currentFragment.findNavController().navigate(R.id.global_go_to_saveInfoUserFragment)
                    } else if (currentFragment is AuthFragment) {
                        currentFragment.goToFragmentWithAnimation(R.id.global_go_to_saveInfoUserFragment)
                    }
                }
            }
            Enums.SatusLogin.LOGIN_FAILED -> {

            }
        }

    }
}