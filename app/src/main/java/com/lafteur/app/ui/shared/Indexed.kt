package com.lafteur.app.ui.shared

interface Indexed {
    val id: String
}