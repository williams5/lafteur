package com.lafteur.app.ui.home.myEvent.event

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.lafteur.app.R
import com.lafteur.app.constants.Constants
import com.lafteur.app.core.list.BaseListAdapter
import com.lafteur.app.core.list.ListFragment
import com.lafteur.app.core.list.base.ListUIConfig
import com.lafteur.app.databinding.FragmentHomeMyEventRequestBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.helpers.extensions.startActivityWithBundle
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.di.DaggerHomeComponent
import com.lafteur.app.ui.newEvent.NewEventActivity


class MyEventRequestFragment : ListFragment<MyEventRequestModel, MyEventRequestViewModel, FragmentHomeMyEventRequestBinding>() {

    override val contentId: Int = R.layout.fragment_home_my_event_request

    override val listViewModel by lazy { getViewModelFragment<MyEventRequestViewModel>() }

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override val recyclerView: RecyclerView?
        get() = dataBinding?.rvEvents

    override fun setupDataBinding() {
        super.setupDataBinding()
        dataBinding?.activityVM = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerHomeComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(activity))
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listViewModel.liveSeeMyAfter.observe(this, Observer {
            listViewModel.eventModel?.let { eventModel ->
                val bundle = Bundle().apply {
                    putSerializable(Constants.BUNDLE_EVENT_MODEL, eventModel)
                }
                activity.startActivityWithBundle<NewEventActivity>(bundle, false)
            }
        })

        listViewModel.liveCreateAfter.observe(this, Observer {
            activityViewModel.liveOnBottomItemSelected.value = R.id.navigation_create_event
        })

    }

    override fun createUIConfig(): ListUIConfig<MyEventRequestModel, MyEventRequestViewModel> =
        object : ListUIConfig<MyEventRequestModel, MyEventRequestViewModel>(this) {
            override val layoutId: Int = R.layout.fragment_home_my_event_request
        }

    override fun createAdapter(): BaseListAdapter<MyEventRequestModel, MyEventRequestViewModel, FragmentHomeMyEventRequestBinding> =
        object : BaseListAdapter<MyEventRequestModel, MyEventRequestViewModel, FragmentHomeMyEventRequestBinding>(this) {
            override fun getCellLayoutId(viewType: Int): Int = R.layout.item_my_event_request

            override fun createItemViewModel(item: MyEventRequestModel, index: Int, type: Int): Any {
                return MyEventRequestItemViewModel(item, index, viewModel)
            }
        }

}