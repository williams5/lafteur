package com.lafteur.app.ui.joinEvent.fragments

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentJoinEventResumeBinding
import com.lafteur.app.helpers.extensions.load
import com.lafteur.app.ui.joinEvent.JoinEventActivity
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import kotlinx.android.synthetic.main.activity_join_event.*
import kotlinx.android.synthetic.main.fragment_join_event_resume.*

class ResumeJoinEventFragment : BaseFragment<FragmentJoinEventResumeBinding>() {

    override val contentId: Int = R.layout.fragment_join_event_resume

    private val activityViewModel by lazy { getViewModelActivity<JoinEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as JoinEventActivity).fullscreenFrag(true)
        activityViewModel.onEventJoined().observe(this, eventJoinedObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iv_participant.load(Uri.parse(activityViewModel.joinEventModel.pictureUri!!))
        tv_name.text = activityViewModel.joinEventModel.owner!!.name
        tv_nb_men.text = activityViewModel.joinEventModel.nbBoy.toString()
        tv_nb_women.text = activityViewModel.joinEventModel.nbGirl.toString()
        tv_description.text = activityViewModel.joinEventModel.description
        tv_phone.text = activityViewModel.joinEventModel.phoneOwner

        btn_update.setOnClickListener {
            findNavController().navigate(R.id.go_to_updateJoinEventFragment)
        }

        btn_close.setOnClickListener {
            activity.showProgressDialog(true)
            activityViewModel.joinEvent()
        }
        /*
        requireActivity().onBackPressedDispatcher.addCallback {
            if(newEventViewModel.isUpdateMode){

            }
        }
*/
        if (activityViewModel.isUpdateMode) {
            (activity as JoinEventActivity).toolbar.hideBack()
        }

    }

    override fun onStop() {
        super.onStop()
        (activity as JoinEventActivity).toolbar.showBack()
        (activity as JoinEventActivity).fullscreenFrag(false)
    }

    private val eventJoinedObserver = Observer<Boolean> { eventJoined ->
        if (eventJoined) {
            activity.hideProgressDialog()
            activity.finish()
        } else {
            activity.showErrorFlashBar("Malheureusement vous n'avez pu rejoindre votre evenement !")
        }
    }

}