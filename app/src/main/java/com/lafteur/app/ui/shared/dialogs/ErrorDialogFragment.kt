package com.lafteur.app.ui.shared.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lafteur.app.databinding.DialogErrorBinding


open class ErrorDialogFragment : AppCompatDialogFragment() {

    //ViewModel will be create before dialog show is called
    private val viewModel by lazy { getViewModel<ErrorViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewModel.apply {
            actionEvent.observe(this@ErrorDialogFragment, Observer {
                dismissAllowingStateLoss()
            })
            arguments?.run {
                isCancelable = getBoolean(CANCELABLE, true)
                init(
                    getInt(BUTTON_TEXT),
                    getString(MESSAGE, "")
                )
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DialogErrorBinding
            .inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                vm = viewModel
            }
            .root
    }

    companion object {
        private const val BUTTON_TEXT = "buttonText"
        private const val MESSAGE = "message"
        private const val CANCELABLE = "cancelable"


        fun newInstance(@StringRes btnTextResId: Int, message: String, cancelable: Boolean = true): ErrorDialogFragment {

            return ErrorDialogFragment().apply {
                arguments = bundleOf(
                    BUTTON_TEXT to btnTextResId,
                    MESSAGE to message,
                    CANCELABLE to cancelable
                )
            }
        }
    }


    private inline fun <reified VM : ViewModel> getViewModel() =
        if (parentFragment != null)
            ViewModelProvider(this).get(VM::class.java)
        else
            ViewModelProvider(activity as AppCompatActivity).get(VM::class.java)

}