package com.lafteur.app.ui.authentification.fragment

import android.os.Bundle
import android.view.View
import com.google.firebase.auth.PhoneAuthProvider
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentAuthVerifPhoneBinding
import com.lafteur.app.helpers.extensions.showKeyboard
import com.lafteur.app.ui.authentification.AuthViewModel
import kotlinx.android.synthetic.main.fragment_auth_verif_phone.*
import org.jetbrains.anko.toast
import kotlin.reflect.KClass

class VerifPhoneFragment : BaseFragment<FragmentAuthVerifPhoneBinding>() {

    override val contentId: Int = R.layout.fragment_auth_verif_phone

    private val activityViewModel: AuthViewModel by lazy { getViewModelActivity<AuthViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM  = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_next.setOnClickListener {
            if (et_pin.length() == 6) {
                btn_next.startAnimation()
                verifyPhoneNumberWithCode(et_pin.text.toString())
            } else {
                activity.toast("Code invalide")
            }
        }

        btn_resend.setOnClickListener {
            activityViewModel.startPhoneVerification(activity)
        }
    }

    private fun verifyPhoneNumberWithCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(activityViewModel.storedVerificationId!!, code)
        activityViewModel.handlePhoneLogin(credential)
    }


    override fun onStop() {
        super.onStop()
        btn_next.revertAnimation()
    }

    override fun onResume() {
        super.onResume()
        et_pin.requestFocus()
        et_pin.showKeyboard()
    }
}