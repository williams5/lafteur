package com.lafteur.app.ui.joinEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentJoinEventPhoneOwnerBinding
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import kotlinx.android.synthetic.main.fragment_join_event_phone_owner.*

class PhoneJoinEventFragment : BaseFragment<FragmentJoinEventPhoneOwnerBinding>() {


    override val contentId: Int = R.layout.fragment_join_event_phone_owner

    private val activityViewModel by lazy { getViewModelActivity<JoinEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityViewModel.joinEventModel.phoneOwner?.let {
            et_phone.text = it
        }

        btn_next.setOnClickListener {
            activityViewModel.updateJoinEventModel(phoneOwner = et_phone.text)
            findNavController().navigate(R.id.go_to_resumeJoinEventFragment)
        }

    }
}