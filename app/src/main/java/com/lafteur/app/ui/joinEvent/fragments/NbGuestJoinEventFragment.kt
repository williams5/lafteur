package com.lafteur.app.ui.joinEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentJoinEventNbGuestBinding
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import com.lafteur.app.ui.shared.DefaultNmberPickerListener
import kotlinx.android.synthetic.main.fragment_join_event_nb_guest.*

class NbGuestJoinEventFragment : BaseFragment<FragmentJoinEventNbGuestBinding>() {

    var total = 0

    override val contentId: Int = R.layout.fragment_join_event_nb_guest

    private val activityViewModel by lazy { getViewModelActivity<JoinEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityViewModel.joinEventModel.nbGirl?.let {
            dnpv_girl.setInitValue(it)
        }

        activityViewModel.joinEventModel.nbBoy?.let {
            dnpv_boy.setInitValue(it)
        }

        activityViewModel.joinEventModel.nbUndefined?.let {
            dnpv_undefined.setInitValue(it)
        }


        updateButtonTotal()

        dnpv_girl.listener = object : DefaultNmberPickerListener {
            override fun onValueChange(oldValue: Int, value: Int) {
                activityViewModel.updateJoinEventModel(nbGirl = value)
                updateButtonTotal()
            }
        }

        dnpv_boy.listener = object : DefaultNmberPickerListener {
            override fun onValueChange(oldValue: Int, value: Int) {
                activityViewModel.updateJoinEventModel(nbBoy = value)
                updateButtonTotal()
            }
        }

        dnpv_undefined.listener = object : DefaultNmberPickerListener {
            override fun onValueChange(oldValue: Int, value: Int) {
                activityViewModel.updateJoinEventModel(nbUndefined = value)
                updateButtonTotal()
            }
        }

        btn_next.setOnClickListener {
            if (activityViewModel.isUpdateMode) {
                findNavController().navigate(R.id.go_to_updateJoinEventFragment)
            } else {
                findNavController().navigate(R.id.go_to_descriptionJoinEventFragment)
            }
        }

    }

    fun updateButtonTotal() {
        val total = dnpv_boy.currentValue + dnpv_girl.currentValue + dnpv_undefined.currentValue
        btn_next.isEnabled = total > 0
        btn_next.text = resources.getQuantityString(R.plurals.total_guest_new_event, total, total)
    }

}