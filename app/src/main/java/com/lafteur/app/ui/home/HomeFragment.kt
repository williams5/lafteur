package com.lafteur.app.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentHomeBinding
import com.lafteur.app.ui.home.create.CreateEventFragment
import com.lafteur.app.ui.home.feed.FeedFragment
import com.lafteur.app.ui.home.myEvent.HomeMyEventFragment
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override val contentId: Int = R.layout.fragment_home

    val activityViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (dataBinding?.vpHome?.adapter == null)
            dataBinding?.vpHome?.adapter = object : FragmentStatePagerAdapter(childFragmentManager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
                val listFragment = listOf<Fragment>(FeedFragment(), CreateEventFragment(), HomeMyEventFragment(), Fragment(R.layout.simple))

                override fun getItem(position: Int): Fragment {
                    return listFragment[position]
                }

                override fun getCount(): Int {
                    return listFragment.size
                }
            }
        dataBinding?.vpHome?.offscreenPageLimit = 4

        activityViewModel.liveOnBottomItemSelected.observe(viewLifecycleOwner, liveOnBottomItemSelected)
    }

    private val liveOnBottomItemSelected = Observer<Int> { itemId ->
        when (itemId) {
            R.id.navigation_home -> vp_home.currentItem = 0
            R.id.navigation_create_event -> vp_home.currentItem = 1
            R.id.navigation_my_events -> vp_home.currentItem = 2
            R.id.navigation_profil -> vp_home.currentItem = 3
        }
    }


}