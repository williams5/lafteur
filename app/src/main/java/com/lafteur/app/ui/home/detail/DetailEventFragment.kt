package com.lafteur.app.ui.home.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.Fade
import androidx.transition.TransitionInflater
import androidx.transition.TransitionSet
import com.lafteur.app.R
import com.lafteur.app.constants.Constants
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentDetailEventBinding
import com.lafteur.app.di.GlobalInjectorModule
import com.lafteur.app.helpers.extensions.addListener
import com.lafteur.app.helpers.extensions.launchCropImage
import com.lafteur.app.helpers.extensions.startActivityWithBundle
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.di.DaggerHomeComponent
import com.lafteur.app.ui.joinEvent.JoinEventActivity
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_detail_event.*

class DetailEventFragment : BaseFragment<FragmentDetailEventBinding>() {

    override val contentId: Int = R.layout.fragment_detail_event

    val viewModel by lazy { getViewModelFragment<DetailEventViewModel>() }

    val activityViewModel: HomeViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.vm = viewModel
        dataBinding?.activityVM = activityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerHomeComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(activity))
            .build()
            .inject(this)
        postponeEnterTransition()

        sharedElementReturnTransition = TransitionInflater
            .from(context)
            .inflateTransition(R.transition.shared_event)
            .setDuration(0)
        exitTransition = Fade()
        enterTransition = Fade()

        (sharedElementEnterTransition as TransitionSet).addListener(
            onEnd = {
                view_filter.animate().alpha(1f).setDuration(resources.getInteger(R.integer.duration_default_anim).toLong()).start()
                slidewDetailFromBottomToTop()
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startPostponedEnterTransition()
        btn_go.setOnClickListener {
            activityViewModel.selectedEvent.get()?.let {
                val bundle = Bundle()
                bundle.putString(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT, "https://image.shutterstock.com/image-photo/panoramic-beautiful-deep-forest-waterfall-600w-1219683721.jpg")
                bundle.putString(Constants.BUNDLE_DESCRIPTION_EVENT,it.description)
                bundle.putString(Constants.BUNDLE_ID_EVENT, it.id)
                activity.startActivityWithBundle<JoinEventActivity>(bundle, false)
            }
            //launchCropImage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        activityViewModel.selectedEvent.get()?.let {
                            val result = CropImage.getActivityResult(data)
                            val bundle = Bundle()
                            bundle.putString(Constants.BUNDLE_URI_PICTURE_CREATE_EVENT, "https://image.shutterstock.com/image-photo/panoramic-beautiful-deep-forest-waterfall-600w-1219683721.jpg")
                            bundle.putString(Constants.BUNDLE_DESCRIPTION_EVENT,it.description)
                            bundle.putString(Constants.BUNDLE_ID_EVENT, it.id)
                            activity.startActivityWithBundle<JoinEventActivity>(bundle, false)
                        }
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {

                    }
                    else -> {

                    }
                }
            }
        }
    }


    private fun slidewDetailFromBottomToTop() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(root)
        constraintSet.clear(v_background_detail.id, ConstraintSet.TOP)
        constraintSet.connect(v_background_detail.id, ConstraintSet.TOP, space.id, ConstraintSet.BOTTOM)
        val transition = ChangeBounds()
        transition.duration = resources.getInteger(R.integer.duration_default_anim).toLong()
        transition.interpolator = DecelerateInterpolator()
        TransitionManager.beginDelayedTransition(root, transition)
        constraintSet.applyTo(root)
    }

}