package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventDateBeginBinding
import com.lafteur.app.helpers.DateTimeHelper
import com.lafteur.app.ui.newEvent.NewEventViewModel
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_new_event_date_begin.*
import org.joda.time.DateTime

class DateBeginEventFragment : BaseFragment<FragmentNewEventDateBeginBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_date_begin

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btn_now.setOnClickListener {
            updateAndGo(DateTime())
        }

        btn_later.setOnClickListener {

            DateTimeHelper.buildDatePickerDialog { dateString ->

                DateTimeHelper.buildTimePickerDialog { timeString ->

                    DateTimeHelper.generateDateTime(dateString, timeString).let {
                        Logger.d(it)
                        updateAndGo(it)
                    }

                }.show(childFragmentManager, "TPD")

            }.show(childFragmentManager, "DPD")
        }
    }


    private fun updateAndGo(dateTime: DateTime) {
        activityViewModel.updateEventModel(dateBegin = dateTime)
        findNavController().navigate(R.id.go_to_dateEndEventFragment)
    }


}