package com.lafteur.app.ui.shared.dialogs

import androidx.annotation.StringRes
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.lafteur.app.helpers.livedata.SingleLiveEvent
import javax.inject.Inject

class ErrorViewModel @Inject constructor() : ViewModel() {

    val title = ObservableField<String>()

    val btnTextResId = ObservableInt()

    val message = ObservableField<String>()

    val actionEvent = SingleLiveEvent<Boolean>()

    fun init(@StringRes btnTextResId: Int, message: String) {
        this.btnTextResId.set(btnTextResId)
        this.message.set(message)
    }

    fun performAction(ok: Boolean) {
        actionEvent.value = ok
    }
}