package com.lafteur.app.ui.home.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentHomeCreateEventBinding
import com.lafteur.app.ui.home.HomeViewModel
import kotlin.reflect.KClass

class ProfileFragment : BaseFragment<FragmentHomeCreateEventBinding>() {

    override val contentId: Int = R.layout.fragment_home_create_event

    val activityViewModel: HomeViewModel by lazy { getViewModelActivity<HomeViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

}