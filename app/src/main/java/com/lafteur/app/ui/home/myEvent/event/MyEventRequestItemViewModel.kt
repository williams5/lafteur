package com.lafteur.app.ui.home.myEvent.event


interface MyEventRequestItemActionListener {
    fun openEvent(myEventRequestModel: MyEventRequestModel, position: Int)
    fun onImageLoaded()
}

class MyEventRequestItemViewModel(val event: MyEventRequestModel, private val position: Int, private val actionListener: MyEventRequestItemActionListener) {

    fun onImageLoaded() {
        if (position == 1)
            actionListener.onImageLoaded()
    }

    fun openEvent() {
        actionListener.openEvent(event, position)
    }

}