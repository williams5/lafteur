package com.lafteur.app.ui.newEvent.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.databinding.FragmentNewEventDateEndBinding
import com.lafteur.app.helpers.DateTimeHelper
import com.lafteur.app.ui.newEvent.NewEventViewModel
import com.lafteur.app.ui.shared.dialogs.ErrorDialogFragment
import com.lafteur.app.ui.shared.dialogs.ErrorViewModel
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_new_event_date_end.*
import org.joda.time.DateTime

class DateEndEventFragment : BaseFragment<FragmentNewEventDateEndBinding>() {

    override val contentId: Int = R.layout.fragment_new_event_date_end

    private val activityViewModel: NewEventViewModel by lazy { getViewModelActivity<NewEventViewModel>() }

    private val errorViewModel by lazy { getViewModelActivity<ErrorViewModel>() }

    override fun setupDataBinding() {
        dataBinding?.activityVM = activityViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_never.setOnClickListener {
            updateAndGo(DateTime().plusYears(1))
        }

        btn_date_end.setOnClickListener {
            DateTimeHelper.buildDatePickerDialog { dateString ->

                val dpd = DateTimeHelper.buildTimePickerDialog { timeString ->

                    DateTimeHelper.generateDateTime(dateString, timeString).let {
                        Logger.d(it)
                        if (activityViewModel.eventModel.dateBegin!!.minusHours(4).isBefore(it)) {

                            ErrorDialogFragment.newInstance(R.string.agree, getString(R.string.error_date)).show(childFragmentManager, null)
                        }
                        else {
                            updateAndGo(it)
                        }
                    }
                }
                dpd.show(childFragmentManager, "TPD")

            }.show(childFragmentManager, "DPD")


        }

        errorViewModel.actionEvent.observe(viewLifecycleOwner, Observer {
            btn_date_end.performClick()
        })

    }

    private fun updateAndGo(dateTime: DateTime) {
        activityViewModel.updateEventModel(dateEnd = dateTime)
        findNavController().navigate(R.id.go_to_resumeDateEventFragment)
    }

}