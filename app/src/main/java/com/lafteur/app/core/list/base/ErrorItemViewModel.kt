package com.lafteur.app.core.list.base

import java.io.IOException

interface ErrorItemActionListener {
    fun retryLoading(refresh: Boolean)
}

class ErrorItemViewModel(exception: Throwable, private val actionListener: ErrorItemActionListener) {

    val isNetworkError = exception is IOException

    fun retryLoading() = actionListener.retryLoading(false)
}