package com.lafteur.app.core.list

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.logger.Logger


/**
 * Scroll listener, used to find :
 * - which data are visible
 * - if we need to load the next page
 * - if the list is at its top (unscrolled)
 */
class ListScrollListener(
    private val viewModel: PagedListViewModel<*>,
    private val preloadThreshold: Int = DEFAULT_THRESHOLD
) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dx != 0 || dy != 0) {
            viewModel.listLiveData
            checkLoadMore(recyclerView)
        }
    }

    /**
     * Check if we need to load more data
     */
    fun checkLoadMore(recyclerView: RecyclerView?) {
        if (canLoadMore(recyclerView)) {
            Logger.d("Scroll more")
            viewModel.loadNextData()


        }
    }

    /**
     * Checks whether the last visible data is close enough to the last loaded data to need loading
     * of the next page
     */
    private fun RecyclerView.isAfterThreshold(): Boolean {
        return (layoutManager as? LinearLayoutManager)?.let { layoutManager ->
            adapter?.let { adapter ->
                layoutManager.findLastVisibleItemPosition() >= adapter.itemCount - preloadThreshold
            }
        } == true
    }

    private fun canLoadMore(recyclerView: RecyclerView?): Boolean {
        return viewModel.loaded.value == true
            && viewModel.hasNextPage.value == true
            && recyclerView?.isAfterThreshold() == true
    }

    companion object {
        const val DEFAULT_THRESHOLD = 5
    }
}