/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.core

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.transition.TransitionInflater
import com.lafteur.app.R
import com.lafteur.app.helpers.extensions.KeyedViewModelLazy
import javax.inject.Inject

abstract class TestFragment<DB : ViewDataBinding>(private val withTransition: Boolean = false) : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @get:LayoutRes
    abstract val contentId: Int

    lateinit var activity: BaseActivity<*, *>

    protected var dataBinding: DB? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = requireActivity() as BaseActivity<*, *>
    }

    open fun initViewModels(viewModel: ViewModel, viewModelActivity: ViewModel) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(contentId, container, false)
        dataBinding = try {
            DataBindingUtil
                .bind<DB>(rootView)
                ?.apply {
                    lifecycleOwner = this@TestFragment
                }
        } catch (e: IllegalArgumentException) {
            null
        }
        setupDataBinding()
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(false)
        if (withTransition) {
            sharedElementReturnTransition = TransitionInflater
                .from(context)
                .inflateTransition(R.transition.shared_event)
                .setDuration(resources.getInteger(R.integer.duration_anim_auth).toLong())
            sharedElementReturnTransition = TransitionInflater
                .from(context)
                .inflateTransition(R.transition.shared_event)
                .setDuration(3000)
        }

    }

    abstract fun setupDataBinding()

    inline fun <reified VM : ViewModel> getViewModelFragment() =
        ViewModelProvider(this, viewModelFactory).get(VM::class.java)

    inline fun <reified VM : ViewModel> getViewModelActivity() =
        ViewModelProvider(activity).get(VM::class.java)

    inline fun <reified VM : ViewModel> Fragment.viewModel(fromActivity: Boolean) =
        KeyedViewModelLazy(this, fromActivity, VM::class, this@TestFragment.viewModelFactory)
}