package com.lafteur.app.core.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.lafteur.app.core.list.base.BaseListViewModel
import com.lafteur.app.core.list.base.Container
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.ui.shared.Indexed
import com.orhanobut.logger.Logger

@Suppress("UNCHECKED_CAST")
abstract class PagedListViewModel<T> : BaseListViewModel<T>() {

    abstract val listLiveData: LiveData<List<T>>

    abstract fun loadNextData()

    abstract val hasNextPage: LiveData<Boolean>

    val isFirstLoading by lazy {
        liveLoadindState.map {
            Logger.d(it.toString())
            when (it) {
                ProgressStatus.FIRST_LOADING -> true
                else -> false
            }
        }
    }


    override fun getLiveListData(): LiveData<List<Container<T>>> {
        return listLiveData.map { list ->
            list.map { item -> item.wrapInContainer() }.also {
                Logger.d("List is upadate ${it.map { (it.content as Indexed).id }}")
                noData.set(list.isEmpty())
            }
        }
    }


}

