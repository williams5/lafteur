package com.lafteur.app.core.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.OrientationHelper
import com.lafteur.app.R
import com.lafteur.app.core.list.base.*


/**
 * Default list adapter. This extends [ListAdapter], so all `notify` operations are computed
 * automatically. This handles progress and error cells, and padding footer.
 */
abstract class BaseListAdapter<T, LVM : BaseListViewModel<T>, DB : ViewDataBinding>(
    context: Context,
    private val lifecycleOwner: LifecycleOwner,
    protected val viewModel: LVM,
    uiConfig: ListUIConfig<*, *>
) : ListAdapter<Container<*>, BindingViewHolder>(ItemComparator(viewModel)) {

    constructor(fragment: ListFragment<T, LVM, DB>) : this(
        fragment.requireContext(),
        fragment.viewLifecycleOwner,
        fragment.listViewModel,
        fragment.uiConfig

    )

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    /**
     * Override this to add a padding footer at the end of the list
     */
    protected open val paddingFooterSize: Int = 0

    /**
     * Override this to specify the progress layout
     */
    @LayoutRes
    protected open val footerProgressLayoutId: Int =
        if (uiConfig.orientation == OrientationHelper.VERTICAL)
            R.layout.item_progress
        else
            R.layout.item_progress_horizontal

    /**
     * Override this to specify the error layout
     */
    @LayoutRes
    protected open val errorLayoutId: Int = R.layout.item_progress

    /**
     * Returns the layout to use for the specific [viewType]
     */
    @LayoutRes
    abstract fun getCellLayoutId(viewType: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        val layoutId = when (viewType) {
            R.id.footer_progress_view_type -> footerProgressLayoutId
            R.id.error_view_type -> errorLayoutId
            else -> getCellLayoutId(viewType)
        }

        // Inflates the cell, and its binding if necessary
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            layoutId,
            parent,
            false
        )
        return if (binding == null) {
            BindingViewHolder(layoutInflater.inflate(layoutId, parent, false))
        } else {
            BindingViewHolder(binding, lifecycleOwner)
        }
    }

    /**
     * Creates the item view model associated with the cell
     */
    abstract fun createItemViewModel(item: T, index: Int, type: Int): Any

    @Suppress("UNCHECKED_CAST")
    fun <E> getContainer(position: Int): Container<E> {
        return super.getItem(position) as Container<E>
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        if (holder.itemViewType in viewModel.classicTypes) {
            val itemViewModel = getContainer<T>(position).let { container ->
                createItemViewModel(container.content, position, container.type)
            }
            holder.bind(itemViewModel)
        } else if (holder.itemViewType == R.id.error_view_type && viewModel is ErrorItemActionListener) {
            holder.bind(ErrorItemViewModel(getContainer<Throwable>(position).content, viewModel))
        }
    }

    override fun onViewRecycled(holder: BindingViewHolder) = holder.onRecycled()

    override fun getItemViewType(position: Int) = getItem(position).type

    public override fun getItem(position: Int): Container<*> {
        return super.getItem(position)
    }
}