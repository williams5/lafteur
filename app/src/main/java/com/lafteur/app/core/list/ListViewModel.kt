package com.lafteur.app.core.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.lafteur.app.core.list.base.BaseListViewModel
import com.lafteur.app.core.list.base.Container
import com.lafteur.app.core.list.base.ProgressStatus
import com.lafteur.app.helpers.livedata.changeValue

@Suppress("UNCHECKED_CAST")
abstract class ListViewModel<T> : BaseListViewModel<T>() {

   // abstract fun reload() no need for Firestore real time
    
    protected abstract val listLiveData: LiveData<List<T>>

    override fun getLiveListData(): LiveData<List<Container<T>>> {
        liveLoadindState.changeValue(ProgressStatus.LOADING)
        return listLiveData.map { list ->
            list.map { item -> item.wrapInContainer() }.also {
                noData.set(list.isEmpty())
                liveLoadindState.changeValue(ProgressStatus.LOADED)
            }
        }
    }

}

