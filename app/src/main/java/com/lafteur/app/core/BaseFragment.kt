/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.core

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.Fade
import androidx.transition.TransitionInflater
import com.lafteur.app.R
import com.lafteur.app.helpers.CustomTransition
import com.lafteur.app.helpers.extensions.KeyedViewModelLazy
import javax.inject.Inject


abstract class BaseFragment<DB : ViewDataBinding> : Fragment() {
    
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    
    @get:LayoutRes
    abstract val contentId: Int

    lateinit var activity: BaseActivity<*, *>

    protected var dataBinding: DB? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = requireActivity() as BaseActivity<*, *>
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (dataBinding == null) {
            dataBinding = try {
                DataBindingUtil
                    .bind<DB>(inflater.inflate(contentId, container, false))
                    ?.apply {
                        lifecycleOwner = this@BaseFragment
                    }
            } catch (e: IllegalArgumentException) {
                null
            }
            setupDataBinding()
        }

        return dataBinding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (dataBinding?.root?.parent != null)
            (dataBinding?.root?.parent as ViewGroup).removeView(dataBinding?.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(false)

        sharedElementEnterTransition = TransitionInflater
            .from(context)
            .inflateTransition(android.R.transition.move)
            .setDuration(resources.getInteger(R.integer.duration_default_anim).toLong())
        sharedElementReturnTransition = TransitionInflater
            .from(context)
            .inflateTransition(R.transition.shared_event)
            .setDuration(resources.getInteger(R.integer.duration_default_anim).toLong())
/*
        sharedElementEnterTransition =  CustomTransition()
        enterTransition = Fade()
        exitTransition = Fade()
        sharedElementReturnTransition = CustomTransition()

 */

    }

    fun goToFragmentWithAnimation(idAction: Int, vararg pair: Pair<View, String>) {
        val extrasSharedElement = FragmentNavigatorExtras(*pair)
        findNavController().navigate(idAction, null, null, extrasSharedElement)
    }

    abstract fun setupDataBinding()

    inline fun <reified VM : ViewModel> getViewModelFragment() =
        ViewModelProvider(this, viewModelFactory).get(VM::class.java)

    inline fun <reified VM : ViewModel> getViewModelActivity() =
        ViewModelProvider(activity).get(VM::class.java)

    inline fun <reified VM : ViewModel> Fragment.viewModel(fromActivity: Boolean) =
        KeyedViewModelLazy(this, fromActivity, VM::class, this@BaseFragment.viewModelFactory)
}