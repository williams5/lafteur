package com.lafteur.app.core.list

import androidx.recyclerview.widget.RecyclerView

open class ScrollListToTopObserver(protected val recyclerView: RecyclerView) : RecyclerView.AdapterDataObserver() {
    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
        if (positionStart == 0 && itemCount > 0) {
            recyclerView.stopScroll()
            recyclerView.scrollToPosition(0)
        }
    }
}