/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.core.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lafteur.app.BR
import com.lafteur.app.core.BaseActivity
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.core.list.base.BaseListViewModel
import com.lafteur.app.core.list.base.ListUIConfig

abstract class ListFragment<T, LVM : BaseListViewModel<T>, DB : ViewDataBinding> : BaseFragment<DB>() {

    val uiConfig by lazy { createUIConfig() }

    protected lateinit var adapter: BaseListAdapter<T, LVM, DB>

    abstract val listViewModel: LVM

    protected abstract val recyclerView: RecyclerView?

    protected open val swipeRefreshLayout: SwipeRefreshLayout? = null

    override fun setupDataBinding() {
        dataBinding?.setVariable(BR.lvm,listViewModel)
        recyclerView?.apply {
            uiConfig.setupRecycler(this, listViewModel, this@ListFragment.adapter)
        }
        swipeRefreshLayout?.apply {
            uiConfig.setupSwipeRefresh(this, listViewModel)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = requireActivity() as BaseActivity<*, *>
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (!this::adapter.isInitialized) {
            adapter = createAdapter()
        }

        listViewModel.getLiveListData().observe(viewLifecycleOwner, Observer { data ->
            adapter.submitList(data)
        })
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    protected abstract fun createUIConfig(): ListUIConfig<T, LVM>

    protected abstract fun createAdapter(): BaseListAdapter<T, LVM, DB>
}