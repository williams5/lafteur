package com.lafteur.app.core.list.base

import android.content.Context
import android.util.SparseIntArray
import androidx.annotation.LayoutRes
import androidx.core.util.isEmpty
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lafteur.app.R
import com.lafteur.app.core.BaseFragment
import com.lafteur.app.core.list.ListScrollListener
import com.lafteur.app.core.list.PagedListViewModel
import com.lafteur.app.core.list.ScrollListToTopObserver
import com.lafteur.app.helpers.binding.addAndTrack

/**
 * This class represents the graphic configuration for the list
 */
abstract class ListUIConfig<T, VM : BaseListViewModel<T>>(protected val context: Context) {

    constructor(fragment: BaseFragment<*>) : this(fragment.requireContext())

    abstract val layoutId: Int
        @LayoutRes get

    protected open val canRefresh = false

    open val orientation = OrientationHelper.VERTICAL

    protected open val itemAnimator: RecyclerView.ItemAnimator? = null

    protected open val itemDecoration: RecyclerView.ItemDecoration? = null

    protected open val nbColumns: Int = 1

    protected open val isReverse = false

    protected open val initialPrefetchCount = 2

    protected open val specialSpanSizes = SparseIntArray()

    protected open val scrollToTopOnLoad = true

    open fun setupRecycler(recyclerView: RecyclerView, viewModel: VM, adapter: RecyclerView.Adapter<*>) {
        recyclerView.layoutManager = createLayoutManager(adapter, viewModel)
        recyclerView.itemAnimator = itemAnimator

        recyclerView.addAndTrack(
            itemDecoration,
            R.id.itemDecoration,
            RecyclerView::addItemDecoration,
            RecyclerView::removeItemDecoration
        )
        if (viewModel is PagedListViewModel<*>) {
            val loadMoreListener = ListScrollListener(viewModel)
            recyclerView.addAndTrack(
                loadMoreListener,
                R.id.listScrollListener,
                RecyclerView::addOnScrollListener,
                RecyclerView::removeOnScrollListener
            )
        }

        recyclerView.adapter = adapter.apply {
            if (scrollToTopOnLoad) {
                adapter.addAndTrack(
                    recyclerView,
                    ScrollListToTopObserver(recyclerView),
                    R.id.scrollToTopListener,
                    RecyclerView.Adapter<*>::registerAdapterDataObserver,
                    RecyclerView.Adapter<*>::unregisterAdapterDataObserver
                )
            }
        }
    }

    fun setupSwipeRefresh(swipeRefreshLayout: SwipeRefreshLayout, viewModel: VM) {
        swipeRefreshLayout.isEnabled = canRefresh
       // swipeRefreshLayout.setOnRefreshListener { viewModel.reload() }
    }

    protected open fun createLayoutManager(adapter: RecyclerView.Adapter<*>, viewModel: BaseListViewModel<T>): RecyclerView.LayoutManager {
        return if (nbColumns == 1) {
            LinearLayoutManager(context, orientation, isReverse).apply {
                initialPrefetchItemCount = initialPrefetchCount
                isSmoothScrollbarEnabled = false
            }
        } else {
            GridLayoutManager(context, nbColumns, orientation, isReverse).apply {
                spanSizeLookup = SpanSizeByHolderIdLookup(specialSpanSizes, nbColumns)
                isSmoothScrollbarEnabled = false
            }
        }
    }

    private class SpanSizeByHolderIdLookup(
        private val specialSpanSizes: SparseIntArray,
        private val nbColumns: Int
    ) : GridLayoutManager.SpanSizeLookup() {

        override fun getSpanSize(position: Int): Int {
            return nbColumns
        }

        override fun getSpanIndex(position: Int, spanCount: Int): Int {
            return if (specialSpanSizes.isEmpty()) {
                position % spanCount
            } else {
                super.getSpanIndex(position, spanCount)
            }
        }
    }
}