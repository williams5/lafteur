package com.lafteur.app.core.list.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.recyclerview.widget.DiffUtil
import com.lafteur.app.R
import com.lafteur.app.core.BaseViewModel
import com.lafteur.app.core.list.ListViewModel
import com.lafteur.app.ui.shared.Indexed


/**
 * Default [DiffUtil.ItemCallback], delegating the comparison to the [ListViewModel]
 */
internal class ItemComparator(private val viewModel: BaseListViewModel<*>) : DiffUtil.ItemCallback<Container<*>>() {
    override fun areItemsTheSame(oldItem: Container<*>, newItem: Container<*>): Boolean {
        return viewModel.areItemsTheSame(oldItem, newItem)
    }

    override fun areContentsTheSame(oldItem: Container<*>, newItem: Container<*>): Boolean {
        return viewModel.areContentsTheSame(oldItem, newItem)
    }
}

@Suppress("UNCHECKED_CAST")
abstract class BaseListViewModel<T> : BaseViewModel(), ErrorItemActionListener {

    open val classicTypes = intArrayOf(R.id.classic_view_type)

    fun areItemsTheSame(oldItem: Container<*>, newItem: Container<*>): Boolean {
        return if (oldItem.type == newItem.type && oldItem.type in classicTypes) {
            (oldItem.content as? T)?.let { old ->
                (newItem.content as? T)?.let { new ->
                    areItemsTheSame(old, new)
                }
            } == true
        } else {
            oldItem == newItem
        }
    }

    /**
     * @see DiffUtil.ItemCallback.areItemsTheSame
     */
    private fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return if (oldItem is Indexed && newItem is Indexed) {
            oldItem.id == newItem.id
        } else {
            oldItem == newItem
        }
    }

    fun areContentsTheSame(oldItem: Container<*>, newItem: Container<*>): Boolean {
        return if (oldItem.type == newItem.type && oldItem.type in classicTypes) {
            (oldItem.content as? T)?.let { old ->
                (newItem.content as? T)?.let { new ->
                    return oldItem == newItem
                }
            } != false
        } else {
            true
        }
    }

    abstract fun getLiveListData(): LiveData<List<Container<T>>>


    val noData = ObservableBoolean()

    abstract var liveLoadindState: MutableLiveData<ProgressStatus> //= MutableLiveData(ProgressStatus.FIRST_LOADING)

     val loaded by lazy {
         liveLoadindState.map {
             when (it) {
                 ProgressStatus.LOADING,ProgressStatus.FIRST_LOADING -> false
                 ProgressStatus.LOADED -> true
                 else -> false
             }
         }
     }


    protected fun T.wrapInContainer(): Container<T> =
        Container(this, R.id.classic_view_type)
}


/**
 * Simple container, used to represent what will be displayed in the list
 *
 * @property content real content
 * @property type content type. This should be a resources id.
 */
data class Container<T>(val content: T, val type: Int = R.id.classic_view_type)

enum class ProgressStatus {
    LOADING,
    LOADED,
    FIRST_LOADING,
    ERROR
}


