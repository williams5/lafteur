package com.lafteur.app.core

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.andrognito.flashbar.Flashbar
import com.lafteur.app.ApiErrorListener
import com.lafteur.app.App
import com.lafteur.app.BR
import com.lafteur.app.R
import com.lafteur.app.helpers.FlashbarHelper
import com.lafteur.app.ui.shared.dialogs.ProgressDialogFragment
import com.lafteur.app.ui.splashscreen.SplashScreenActivity
import com.orhanobut.logger.Logger
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.OnErrorNotImplementedException
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.rxkotlin.subscribeBy
import org.jetbrains.anko.getStackTraceString
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseActivity<VM : BaseViewModel, DB : ViewDataBinding>(private var viewModelClass: KClass<VM>,
                                                                                        @LayoutRes private var contentId: Int) : AppCompatActivity(), ApiErrorListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: VM

    protected var dataBinding: ViewDataBinding? = null

    private lateinit var progressDialog: ProgressDialogFragment
    private var refreshPageIfNetworkBack: Boolean = false
    private lateinit var flashBarNoNetwork: Flashbar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass.java)

        DataBindingUtil
            .setContentView<DB>(this, contentId)
            .apply {
                dataBinding = this
                lifecycleOwner = this@BaseActivity
                setVariable(BR.vm, viewModel)
            }

        (application as App).setInternetConnectionListener(this)

        initializeRxJavaErrorHandler()
        createProgressDialog()
        flashBarNoNetwork = FlashbarHelper.createFlashBarError(this, getString(R.string.flashbar_becarful_no_network), getString(R.string.flashbar_message_no_network))
    }

    override fun onResume() {
        super.onResume()
        if (!App.isDeviceConnected) {
            showFlashBarWithDelay()
        }
        if (!(application as App).hasInternetConnectionListener()) {
            (application as App).setInternetConnectionListener(this)
        }
        if (RxJavaPlugins.getErrorHandler() == null) {
            initializeRxJavaErrorHandler()
        }
        if (refreshPageIfNetworkBack) {
            if (App.isDeviceConnected) {
                finish()
                startActivity(intent)
            } else {
                //lostConnectivityDialog.show(this, "lostConnectivityDialog")
            }
            refreshPageIfNetworkBack = false
        }

    }

    override fun onPause() {
        super.onPause()
        if (!App.isDeviceConnected) {
            refreshPageIfNetworkBack = true
        }
        (application as App).setInternetConnectionListener(null)
        RxJavaPlugins.setErrorHandler(null)
    }

    override fun onInternetAvailable() {
        if (flashBarNoNetwork.isShown()) {
            flashBarNoNetwork.dismiss()
        }
    }

    override fun onInternetUnavailable() {
        showFlashBarWithDelay()
        toast("no network")
    }

    override fun onUnauthorized() {
        /*
        if (this !is LoginActivity && this !is RegisterActivity) {

            HomeAuthActivity.start(this)
            toast("unauthorized")
        }
        */
    }


    private fun createProgressDialog() {
        progressDialog = ProgressDialogFragment.createDialog()
    }

    private fun initializeRxJavaErrorHandler() {

        RxJavaPlugins.setErrorHandler { e ->
            hideProgressDialog()
            var throwable = e

            if (throwable is OnErrorNotImplementedException) {
                throwable = throwable.cause
            }
            if (throwable.localizedMessage == null) {
                Logger.e(throwable.getStackTraceString())
            } else {
                Logger.e(throwable.getStackTraceString())
            }

            Observable.timer(250, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    FlashbarHelper
                        .createFlashBarError(this,
                            getString(R.string.flashbar_becarful_error),
                            getString(R.string.flashbar_message_error))
                        .show()
                }

            //  Crashlytics.logException(throwable)

        }
    }

    fun showProgressDialog(withDim: Boolean = false) {
        supportFragmentManager.executePendingTransactions()
        if (!progressDialog.isAdded) {
            if (withDim) {
                progressDialog.showWithDim(this, "ProgressDialog")
            } else {
                progressDialog.show(this, "ProgressDialog")
            }

        }
    }

    fun hideProgressDialog() {
        supportFragmentManager.executePendingTransactions()
        if (progressDialog.isAdded) {
            // Handler().postDelayed({
            progressDialog.dismiss()
            //  }, 500)
        }
    }


    fun showErrorFlashBar(message: String) {
        FlashbarHelper
            .createFlashBarError(this, getString(R.string.flashbar_becarful_no_network), message)
            .show()
    }

    private fun showFlashBarWithDelay() {
        if (!flashBarNoNetwork.isShown() && this !is SplashScreenActivity) {
            Handler().postDelayed({
                flashBarNoNetwork.show()
            }, 250)
        }
    }
}