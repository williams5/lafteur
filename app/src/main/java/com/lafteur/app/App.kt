package com.lafteur.app

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.ProcessLifecycleOwner
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.lafteur.app.di.AppModule
import com.lafteur.app.di.DaggerAppComponent
import com.lafteur.app.helpers.network.ConnectivityReceiver
import com.lafteur.app.helpers.network.NetworkHelper
import com.lafteur.app.infrastructure.token.TokenJWTService
import net.danlew.android.joda.JodaTimeAndroid
import javax.inject.Inject


class App : Application(), ConnectivityReceiver.ConnectivityReceiverListener, AppLifecycleObserver.AppLifecycleListener,
    ApiErrorListener {

    @Inject
    lateinit var appLifecycleObserver: AppLifecycleObserver

    @Inject
    lateinit var tokenJWTService: TokenJWTService

    private var apiErrorListener: ApiErrorListener? = null

    companion object {
        var isDeviceConnected = false
        var isDeviceBackground = false
    }

    override fun onCreate() {
        super.onCreate()

        //for beautiful logger init here
        Logger.addLogAdapter(AndroidLogAdapter())

        //init jodata time if not no working
        JodaTimeAndroid.init(this)

        //check for first time if connected internet
        App.isDeviceConnected = NetworkHelper.isConnected(applicationContext)

        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
            .inject(this)

        ProcessLifecycleOwner.get().lifecycle.addObserver(appLifecycleObserver)
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    fun isLogged(): Boolean {
        return tokenJWTService.hasTokenValid()
    }

    fun getToken(): String {
        return tokenJWTService.getToken()
    }


    fun setInternetConnectionListener(listener: ApiErrorListener?) {
        apiErrorListener = listener
    }

    fun hasInternetConnectionListener(): Boolean {
        return apiErrorListener != null
    }

    fun removeApiErrorListner() {
        apiErrorListener = null
    }


    override fun onUnauthorized() {
        apiErrorListener?.onUnauthorized()
    }

    override fun onInternetUnavailable() {
        apiErrorListener?.onInternetUnavailable()
    }

    override fun onInternetAvailable() {
        apiErrorListener?.onInternetAvailable()
    }

    override fun onEnteringForeground() {
        isDeviceBackground = false
        observeConnectivity()
    }

    override fun onEnteringBackGround() {
        isDeviceBackground = true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isDeviceConnected = isConnected
        if (!isDeviceConnected) {
            onInternetUnavailable()
        } else {
            onInternetAvailable()
        }
    }

    private fun observeConnectivity() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

}

interface ApiErrorListener {
    fun onInternetUnavailable()
    fun onInternetAvailable()
    fun onUnauthorized()
}