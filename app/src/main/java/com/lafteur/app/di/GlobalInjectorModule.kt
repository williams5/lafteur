/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.di

import android.app.Activity
import android.content.Context
import com.lafteur.app.App
import com.lafteur.app.api.adapter.DateTimeAdapter
import com.lafteur.app.api.data.DataApiService
import com.lafteur.app.api.data.DataApiServiceImpl
import com.lafteur.app.helpers.network.NetworkConnectionInterceptor
import com.lafteur.app.helpers.sharedpreferences.SharedPreferencesHelper
import com.lafteur.app.helpers.sharedpreferences.SharedPreferencesHelperImpl
import com.lafteur.app.helpers.variant.VariantHelper
import com.lafteur.app.helpers.variant.VariantHelperImpl
import com.lafteur.app.infrastructure.event.EventFirestoreRepositoryImpl
import com.lafteur.app.infrastructure.event.EventQueryFactory
import com.lafteur.app.infrastructure.event.EventQueryFactoryImpl
import com.lafteur.app.infrastructure.event.EventRepository
import com.lafteur.app.infrastructure.facebook.FacebookUserRepository
import com.lafteur.app.infrastructure.facebook.FacebookUserRepositoryImpl
import com.lafteur.app.infrastructure.token.TokenJWTService
import com.lafteur.app.infrastructure.token.TokenJWTServiceImpl
import com.lafteur.app.infrastructure.upload.UploadFirebaseRepositoryImpl
import com.lafteur.app.infrastructure.upload.UploadRepository
import com.lafteur.app.infrastructure.user.UserRepository
import com.lafteur.app.infrastructure.user.UserFirestoreRepositoryImpl
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.jetbrains.anko.runOnUiThread
import org.joda.time.DateTime
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@Module
class GlobalInjectorModule(var activity: Activity) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideApplication(): App = activity.application as App

    /* Helper */
    @Provides
    fun provideSharedPreferencesHelper(helper: SharedPreferencesHelperImpl): SharedPreferencesHelper = helper

    @Provides
    fun provideVariantHelper(helper: VariantHelperImpl): VariantHelper = helper

    /* Repo */

    @Provides
    fun provideUserFirestoreRepository(repository: UserFirestoreRepositoryImpl): UserRepository = repository

    @Provides
    fun provideEventRepository(repository: EventFirestoreRepositoryImpl): EventRepository = repository

    @Provides
    fun provideFacebookUserRepository(repository: FacebookUserRepositoryImpl): FacebookUserRepository = repository

    @Provides
    fun provideUploadRepository(repository: UploadFirebaseRepositoryImpl): UploadRepository = repository

    /* Service */
    @Provides
    fun provideTokenJWTService(service: TokenJWTServiceImpl): TokenJWTService = service

    /* Factory */

    @Provides
    fun provideEventQueryFactory(factory: EventQueryFactoryImpl): EventQueryFactory = factory

    //API
    @Provides
    fun provideProviderApiService(service: DataApiServiceImpl): DataApiService = service


    /*
    *
    *
    * RETROFIT
    *
    *
    *
     */
    @Provides
    fun provideRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .add(DateTime::class.java, DateTimeAdapter())
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(provideOkHttpClient())
            .baseUrl(provideVariantHelper(VariantHelperImpl()).getBackendEndPoint())
            .build()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        //todo make better
        okhttpClientBuilder.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer " + provideApplication().getToken())
                .build()
            val response = chain.proceed(request)
            if (response.code() == 401 || response.code() == 403) {
                provideContext().runOnUiThread {
                    provideApplication().onUnauthorized()
                }
            }
            response
        }


        okhttpClientBuilder.addInterceptor(object : NetworkConnectionInterceptor() {
            override fun isInternetAvailable(): Boolean {
                return App.isDeviceConnected
            }

            override fun onInternetUnavailable() {
                provideContext().runOnUiThread {
                    provideApplication().onInternetUnavailable()
                }
            }
        })

        return okhttpClientBuilder.build()
    }
}