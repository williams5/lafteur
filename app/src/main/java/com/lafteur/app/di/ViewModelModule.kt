/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lafteur.app.ui.authentification.AuthViewModel
import com.lafteur.app.ui.home.HomeViewModel
import com.lafteur.app.ui.home.detail.DetailEventViewModel
import com.lafteur.app.ui.home.feed.FeedViewModel
import com.lafteur.app.ui.home.myEvent.event.MyEventRequestFragment
import com.lafteur.app.ui.home.myEvent.event.MyEventRequestViewModel
import com.lafteur.app.ui.home.myEvent.request.RequestEventViewModel
import com.lafteur.app.ui.joinEvent.JoinEventViewModel
import com.lafteur.app.ui.splashscreen.SplashScreenViewModel
import com.lafteur.app.ui.newEvent.NewEventViewModel
import com.lafteur.app.ui.shared.dialogs.ErrorViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    internal abstract fun splashScreenViewModel(splashScreenViewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    internal abstract fun authViewModel(authViewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun homeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewEventViewModel::class)
    internal abstract fun newEventViewModel(newEventViewModel: NewEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(JoinEventViewModel::class)
    internal abstract fun joinEventViewModel(joinEventViewModel: JoinEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailEventViewModel::class)
    internal abstract fun detailEventViewModel(detailEventViewModel: DetailEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel::class)
    internal abstract fun feedViewModel(feedViewModel: FeedViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ErrorViewModel::class)
    internal abstract fun errorViewModel(errorViewModel: ErrorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RequestEventViewModel::class)
    internal abstract fun requestEventViewModel(requestEventViewModel: RequestEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyEventRequestViewModel::class)
    internal abstract fun myEventRequestViewModel(myEventRequestViewModel: MyEventRequestViewModel): ViewModel

}
