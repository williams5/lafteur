/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.di

import com.lafteur.app.App
import dagger.Component


@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(application: App)
}