/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.di

import android.content.Context
import com.lafteur.app.App
import com.lafteur.app.AppLifecycleObserver
import com.lafteur.app.helpers.sharedpreferences.SharedPreferencesHelper
import com.lafteur.app.helpers.sharedpreferences.SharedPreferencesHelperImpl
import com.lafteur.app.infrastructure.token.TokenJWTService
import com.lafteur.app.infrastructure.token.TokenJWTServiceImpl
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: App) {

    @Provides
    fun provideApplicationContext(): Context = application

    @Provides
    fun provideAppLifecycleObserver(): AppLifecycleObserver = AppLifecycleObserver(application)

    @Provides
    fun provideSharedPreferencesHelper(): SharedPreferencesHelper = SharedPreferencesHelperImpl()

    @Provides
    fun provideTokenJWTService(service: TokenJWTServiceImpl): TokenJWTService = service
}