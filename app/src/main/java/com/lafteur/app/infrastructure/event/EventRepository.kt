package com.lafteur.app.infrastructure.event

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.Query
import com.lafteur.app.ui.home.models.FeedEventModel
import io.reactivex.Completable

interface EventRepository {

    fun createEvent(event: Event): Completable
    suspend fun jointEvent(idEvent: String, userRequest: UserRequest)
    fun getEvent(uid: String): LiveData<FeedEventModel>
    fun getMyEvent(): LiveData<Event?>
    fun getQueryPagedAllEvent(limit: Int): Query
    fun getQueryPagedEventMyRequest(limit: Int): Query

    companion object {
        val modelClazz = Event::class.java
    }

}

