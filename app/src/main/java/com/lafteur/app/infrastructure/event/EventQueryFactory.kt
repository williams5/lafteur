package com.lafteur.app.infrastructure.event

import com.google.firebase.firestore.Query
import com.lafteur.app.constants.Enums

interface EventQueryFactory {

    fun getLimitItemPerPage(): Int
    fun getInitialQuery(): Query
    fun getClassModel(): Class<Event>
    fun queryPagedForType(eventType: Enums.EventType, limit: Int): EventQueryFactory
}