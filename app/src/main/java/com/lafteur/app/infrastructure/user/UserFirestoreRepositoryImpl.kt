package com.lafteur.app.infrastructure.user

import androidx.lifecycle.LiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.lafteur.app.helpers.livedata.livedata
import com.lafteur.app.infrastructure.exceptions.IsCurrentUserLoggedException
import io.reactivex.Single
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UserFirestoreRepositoryImpl @Inject constructor() : UserRepository {

    private var firestoreDB = FirebaseFirestore.getInstance()

    private val COLLECTION_NAME = "users"

    private fun getUsersCollection(): CollectionReference {
        return firestoreDB.collection(COLLECTION_NAME)
    }

    override fun createUser(user: User): Task<Void> {
        return getUsersCollection().document(user.uid).set(user)
    }

    override fun getLiveUser(uid: String): LiveData<User> {
        return getUsersCollection().document(uid).livedata(User::class.java)
    }

    override suspend fun getMyInfos(): User {
        val currentUser = FirebaseAuth.getInstance().currentUser
        return getUsersCollection().document(currentUser!!.uid).get().await().toObject(User::class.java)!!
    }

    override fun isCurrentUserLogged(): Single<Boolean> = Single.create { emitter ->
        val fbAuth = FirebaseAuth.getInstance()
        val currentUser = fbAuth.currentUser
        if (currentUser != null) {
            getUsersCollection().document(currentUser.uid).get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    if (task.result!!.exists()) {
                        emitter.onSuccess(true)
                    } else {
                        emitter.onSuccess(false)
                    }
                } else {
                    emitter.onError(IsCurrentUserLoggedException())
                }
            }
        } else {
            emitter.onSuccess(false)
        }

    }


}