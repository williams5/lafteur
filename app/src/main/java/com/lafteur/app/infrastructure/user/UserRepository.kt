package com.lafteur.app.infrastructure.user

import androidx.lifecycle.LiveData
import com.google.android.gms.tasks.Task
import io.reactivex.Single

interface UserRepository {

    fun createUser(user: User): Task<Void>
    fun getLiveUser(uid:String):LiveData<User>
    suspend fun getMyInfos(): User
    fun isCurrentUserLogged(): Single<Boolean>
}