package com.lafteur.app.infrastructure.upload

import android.net.Uri
import io.reactivex.Single

interface UploadRepository {

    fun uploadEventImage(uriString: String): Single<String>
}