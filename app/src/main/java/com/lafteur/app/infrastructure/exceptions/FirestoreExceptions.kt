package com.lafteur.app.infrastructure.exceptions

class IsCurrentUserLoggedException : Exception()