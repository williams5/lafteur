package com.lafteur.app.infrastructure.facebook

import android.os.Bundle
import com.facebook.AccessToken
import com.facebook.GraphRequest
import io.reactivex.Single
import javax.inject.Inject

class FacebookUserRepositoryImpl @Inject constructor() : FacebookUserRepository {
    companion object {
        const val FB_USER_ID = "id"
        const val FB_USER_BIRTHDAY = "birthday"
        const val FB_USER_FIRST_NAME = "first_name"
        const val FB_USER_LAST_NAME = "last_name"
        const val FB_USER_GENDER = "gender"
        const val FB_USER_LOCATION = "location"
        const val FB_USER_PICTURE = "picture"
        const val FB_USER_EMAIL = "email"
        const val FB_PHOTO_HEIGHT = "height"
        const val FB_PHOTO_CREATED = "created_time"
        const val FB_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ"
    }

    override fun getInfosUser(): Single<FacebookUserDto> = Single.create { emitter ->
        val request = GraphRequest
            .newMeRequest(AccessToken.getCurrentAccessToken()) { _, response ->
                try {
                    val jsonObject = response.jsonObject
                    val location = jsonObject.getJSONObject(FB_USER_LOCATION).getString("name")
                    val picture = jsonObject.getJSONObject(FB_USER_PICTURE).getJSONObject("data").getString("url")
                    val fbUserDto = FacebookUserDto(jsonObject.getString(FB_USER_ID),
                        jsonObject.getString(FB_USER_FIRST_NAME),
                        jsonObject.getString(FB_USER_LAST_NAME),
                        jsonObject.getString(FB_USER_GENDER),
                        jsonObject.getString(FB_USER_BIRTHDAY),
                        location,
                        picture,
                        jsonObject.getString(FB_USER_EMAIL)
                    )
                    emitter.onSuccess(fbUserDto)
                } catch (exception: Exception) {
                    emitter.onError(exception)
                }
            }

        val parameters = Bundle()
        parameters.putString("fields", "$FB_USER_ID, $FB_USER_BIRTHDAY, $FB_USER_FIRST_NAME,$FB_USER_LAST_NAME,$FB_USER_GENDER,$FB_USER_LOCATION, $FB_USER_PICTURE.type(large), $FB_USER_EMAIL")
        request.parameters = parameters
        request.executeAndWait()

    }


}