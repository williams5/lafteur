package com.lafteur.app.infrastructure.facebook

import io.reactivex.Single

interface FacebookUserRepository {

    fun getInfosUser(): Single<FacebookUserDto>
}