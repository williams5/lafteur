package com.lafteur.app.infrastructure.event

import com.google.firebase.firestore.Query
import com.lafteur.app.constants.Enums
import java.lang.Exception
import javax.inject.Inject

class EventQueryFactoryImpl @Inject constructor(private val eventRepository: EventRepository) : EventQueryFactory {

     var query: Query? = null

    private var maxItemPerPage: Int = 10

    override fun getLimitItemPerPage(): Int {
        return maxItemPerPage
    }

    override fun getInitialQuery(): Query {
        return if(query != null)
            query!!
        else
            throw Exception("Query must be initialized call queryPagedForType")
    }


    override fun getClassModel(): Class<Event> {
        return Event::class.java
    }

    override fun queryPagedForType(eventType: Enums.EventType, limit: Int): EventQueryFactory {
        maxItemPerPage = limit
        query = when (eventType) {
            Enums.EventType.REQUEST_EVENT -> eventRepository.getQueryPagedEventMyRequest(limit)
            Enums.EventType.ALL_EVENT -> eventRepository.getQueryPagedAllEvent(limit)
        }
        return this
    }


}