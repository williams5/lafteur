package com.lafteur.app.infrastructure.upload

import android.net.Uri
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Single
import java.util.UUID
import javax.inject.Inject

class UploadFirebaseRepositoryImpl @Inject constructor() : UploadRepository {

    private val STORAGE_NAME = "events"

    override fun uploadEventImage(uriString: String): Single<String> = Single.create { emitter ->
        val storageRef = FirebaseStorage.getInstance().reference
        val eventRef = storageRef.child(STORAGE_NAME)
        val generateUUID = UUID.randomUUID().toString()
        val imageEventRef = eventRef.child(generateUUID)

        imageEventRef.putFile(Uri.parse(uriString)).continueWithTask { task ->
            if (!task.isSuccessful) {
                emitter.onError(task.exception!!)
            }
            imageEventRef.downloadUrl
        }.addOnCompleteListener { taskUri ->
            if (taskUri.isSuccessful) {
                emitter.onSuccess(taskUri.result!!.toString())
            } else {
                emitter.onError(taskUri.exception!!)
            }
        }
    }
}