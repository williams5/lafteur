package com.lafteur.app.infrastructure.event

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.lafteur.app.helpers.livedata.livedata
import com.lafteur.app.helpers.livedata.singleLivedata
import com.lafteur.app.ui.home.models.FeedEventModel
import com.orhanobut.logger.Logger
import io.reactivex.Completable
import javax.inject.Inject

class EventFirestoreRepositoryImpl @Inject constructor() : EventRepository {

    private var firestoreDB = FirebaseFirestore.getInstance()

    private val COLLECTION_NAME = "events"
    private val DATE_BEGIN_NAME = "dateBegin"
    private val ARRAY_ID_USER_REQUEST_NAME = "idUserRequest"
    private val ID_OWNER_NAME = "uidOwner"

    private fun getEventsCollection(): CollectionReference {
        return firestoreDB.collection(COLLECTION_NAME)
    }

    override fun getQueryPagedAllEvent(limit: Int) =
        getEventsCollection().orderBy(DATE_BEGIN_NAME, Query.Direction.ASCENDING).limit(limit.toLong())

    override fun getQueryPagedEventMyRequest(limit: Int): Query {
        val currentUser = FirebaseAuth.getInstance().currentUser
        try {
            return getEventsCollection().orderBy(DATE_BEGIN_NAME, Query.Direction.ASCENDING).limit(limit.toLong()).whereArrayContains(ARRAY_ID_USER_REQUEST_NAME, currentUser!!.uid)
        } catch (e: Exception) {
            throw e
        }
    }


    override suspend fun jointEvent(idEvent: String, userRequest: UserRequest) {
        //val  userRequest = hashMapOf("userRequest" to arrayListOf(jointEventModel))
        try {
            getEventsCollection().document(idEvent).update(ARRAY_ID_USER_REQUEST_NAME, FieldValue.arrayUnion(userRequest.idUser))
            getEventsCollection().document(idEvent).update("userRequest", FieldValue.arrayUnion(userRequest))
        } catch (e: Exception) {
            Logger.e(e.localizedMessage)
        }

    }

    override fun createEvent(event: Event): Completable = Completable.create { emitter ->
        getEventsCollection().document().set(event).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                emitter.onComplete()
            } else {
                emitter.onError(task.exception!!)
            }
        }
    }

    override fun getEvent(uid: String): LiveData<FeedEventModel> {
        return getEventsCollection().document(uid).livedata(Event::class.java).map { event ->
            event.toFeedEventModel()
        }
    }

    override fun getMyEvent(): LiveData<Event?> {
        val currentUser = FirebaseAuth.getInstance().currentUser
        try {
            return getEventsCollection().whereEqualTo(ID_OWNER_NAME, currentUser!!.uid).limit(1).singleLivedata(Event::class.java)
        } catch (e: Exception) {
            throw e
        }
    }


    private fun Event.toFeedEventModel(): FeedEventModel {
        return FeedEventModel(uid,
            nameOwner,
            picture,
            nbCurrentGuest,
            nbAcceptedGuest,
            dateBegin,
            dateEnd,
            description,
            addressOwner,
            phoneOwner)
    }

}