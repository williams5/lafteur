package com.lafteur.app.infrastructure.facebook

data class FacebookUserDto(val id: String,
                           val firstName: String,
                           val lastName: String,
                           val gender: String,
                           val birthday: String,
                           val location: String,
                           val picture: String,
                           val email: String) {
}

