package com.lafteur.app.infrastructure.event

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import com.lafteur.app.helpers.firestore.FirestorePagedData

data class Event (
    @DocumentId
    val uid: String,
    val addressOwner: String = "address de de test",
    val dateBegin: Timestamp = Timestamp.now(),
    val dateEnd: Timestamp = Timestamp.now(),
    val description: String = " la poule",
    val nameOwner: String = "test name",
    val nbAcceptedGuest: Int = 2,
    val nbCurrentGuest: Int = 2,
    val phoneOwner: String = "00000000",
    val picture: String = "https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg",
    val uidOwner: String = "han han",
    val userRequest: List<UserRequest> = listOf()): FirestorePagedData {


    constructor() : this("",
        "",
        Timestamp.now(),
        Timestamp.now(),
        "",
        "",
        0,
        0,
        "",
        "",
        "",
        listOf())

    constructor(addressOwner: String,
                dateBegin: Timestamp,
                dateEnd: Timestamp,
                description: String,
                nameOwner: String,
                nbAcceptedGuest: Int,
                nbCurrentGuest: Int,
                phoneOwner: String,
                picture: String,
                uidOwner: String) :
        this("",
            addressOwner,
            dateBegin,
            dateEnd,
            description,
            nameOwner,
            nbAcceptedGuest,
            nbCurrentGuest,
            phoneOwner,
            picture,
            uidOwner,
            listOf())

    override fun getId(): String {
        return uid
    }

    override fun getComparableDate(): Timestamp {
        return dateBegin
    }

}

data class UserRequest(var idUser: String,
                       var nameUser: String,
                       var phoneOwner: String,
                       var pictureUri: String,
                       var nbBoy: Int,
                       var nbGirl: Int,
                       var nbUndefined: Int,
                       var description: String,
                       var stateRequest: String) {
    constructor() : this("",
        "",
        "",
        "",
        0,
        0,
        0,
        "",
        "")
}


