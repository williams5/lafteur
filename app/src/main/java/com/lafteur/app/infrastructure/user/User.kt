package com.lafteur.app.infrastructure.user

data class User(val uid: String, val loginBy: String, var name: String, var birthday: String, var phone: String) {
    constructor() : this("", "", "", "", "")
    constructor(uid: String, loginBy: String) : this(uid, loginBy, "", "", "")
}