/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.api.data

import android.content.Context
import com.lafteur.app.api.ApiClientImpl
import com.lafteur.app.api.adapter.CompletableEmitterErrorAdapter
import com.lafteur.app.api.adapter.EmitterErrorAdapter
import com.lafteur.app.api.models.DataTempDto
import com.lafteur.app.helpers.extensions.toRequestBody
import com.orhanobut.logger.Logger
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class DataApiServiceImpl @Inject constructor(val context: Context, retrofit: Retrofit) :
    DataApiService, ApiClientImpl() {


    private var dataApi: DataApi = retrofit.create(DataApi::class.java)
    private val TAG = "DataApiService"


    override fun getDataTemp(): Single<DataTempDto> = Single.create { emitter ->
        dataApi.getDataTemp()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { response ->
                    if (response.isSuccessful) {
                        emitter.onSuccess(response.body()!!)
                    } else {
                        handleStatusCodeToSendEmitter(EmitterErrorAdapter(emitter), response.code())
                    }
                },
                onError = {
                    emitter.onError(it)
                    Logger.e(it.localizedMessage)
                }
            )
    }


    override fun postData(data: DataTempDto): Completable = Completable.create { emitter ->
        dataApi.postData(data.id.toRequestBody(), data.dataNameTemp.toRequestBody())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { response ->
                    if (response.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        handleStatusCodeToSendEmitter(CompletableEmitterErrorAdapter(emitter), response.code())
                    }
                },
                onError = {
                    emitter.onError(it)
                    Logger.e(it.localizedMessage)
                }
            )
    }

    override fun updateData(idData: Int, newNameData: String): Completable = Completable.create { emitter ->
        dataApi.updateData(idData, newNameData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { response ->
                    if (response.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        Logger.e(response.errorBody()!!.string())
                        handleStatusCodeToSendEmitter(CompletableEmitterErrorAdapter(emitter), response.code())
                    }
                },
                onError = {
                    emitter.onError(it)
                    Logger.e(it.localizedMessage)
                }
            )
    }

    override fun deleteData(idData: Int): Completable = Completable.create { emitter ->
        dataApi.deleteData(idData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { response ->
                    if (response.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        Logger.e(response.errorBody()!!.string())
                        handleStatusCodeToSendEmitter(CompletableEmitterErrorAdapter(emitter), response.code())
                    }
                },
                onError = {
                    emitter.onError(it)
                    Logger.e(it.localizedMessage)
                }
            )
    }
}