package com.lafteur.app.api.models

import com.squareup.moshi.Json

data class DataTempDto(

    @field:Json(name = "id")
    val id: Int,

    @field:Json(name = "data_temp_name")
    val dataNameTemp: String
)