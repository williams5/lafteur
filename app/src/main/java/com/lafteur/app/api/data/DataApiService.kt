/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.api.data

import com.lafteur.app.api.models.DataTempDto
import io.reactivex.Completable
import io.reactivex.Single

interface DataApiService {

    fun getDataTemp(): Single<DataTempDto>
    fun postData(data: DataTempDto): Completable
    fun updateData(idData: Int, newNameData: String): Completable
    fun deleteData(idData: Int): Completable
}