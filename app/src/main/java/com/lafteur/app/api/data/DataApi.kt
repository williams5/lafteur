/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.api.data

import com.lafteur.app.api.models.DataTempDto
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface DataApi {

    @GET("api/exemple/get_data_temp")
    fun getDataTemp(): Single<Response<DataTempDto>>

    @Headers("X-Requested-With: XMLHttpRequest")
    @Multipart
    @POST("api/exemple/post_data")
    fun postData(@Part("data_id") idData: RequestBody,
                 @Part("data_name") dataName: RequestBody): Single<Response<Any>>

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @PUT("api/exemple/data/{id}")
    fun updateData(@Path("id") idData: Int,
                   @Field("new_data_name") newDataName: String): Single<Response<Any>>

    @Headers("X-Requested-With: XMLHttpRequest")
    @DELETE("api/exemple/data/{id}")
    fun deleteData(@Path("id") idTrip: Int): Single<Response<Any>>

}