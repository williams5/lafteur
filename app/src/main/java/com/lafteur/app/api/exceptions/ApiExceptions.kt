/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.api.exceptions

class BadRequestException : Exception()

class UnauthorizedException : Exception()

class NotFoundException : Exception()

class BadEmailException : Exception()

class ServerErrorException : Exception()

class LostConnectivityException : Exception()
