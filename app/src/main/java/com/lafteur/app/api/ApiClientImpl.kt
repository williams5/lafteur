/*
 * Copyright (c) 2018 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.lafteur.app.api

import android.content.res.Resources
import com.lafteur.app.api.exceptions.BadEmailException
import com.lafteur.app.api.exceptions.BadRequestException
import com.lafteur.app.api.exceptions.ServerErrorException
import com.lafteur.app.api.exceptions.UnauthorizedException
import com.lafteur.app.constants.Enums
import io.reactivex.Emitter

abstract class ApiClientImpl {

    /**
     * observer error Handler to supply to anko async for emit api client error
     */

    fun handleStatusCodeToSendEmitter(emitter: Emitter<*>, statusCode: Int) {
        when (statusCode) {
            Enums.HTTPStatus.NOT_FOUND.code -> onNotFoundRequest(emitter)
            Enums.HTTPStatus.BAD_REQUEST.code -> onBadRequest(emitter)
            Enums.HTTPStatus.BAD_EMAIL.code -> onEmailBadRequest(emitter)
            Enums.HTTPStatus.UNAUTHORIZED.code, Enums.HTTPStatus.FORBIDDEN.code -> onUnauthorizedRequest(emitter)
            Enums.HTTPStatus.INTERNAL_SERVER_ERROR.code -> onServerError(emitter)
        }
    }

    private fun onEmailBadRequest(emitter: Emitter<*>) {
        emitter.onError(BadEmailException())
    }

    private fun onBadRequest(emitter: Emitter<*>) {
        emitter.onError(BadRequestException())
    }

    private fun onUnauthorizedRequest(emitter: Emitter<*>) {
        emitter.onError(UnauthorizedException())
    }

    private fun onServerError(emitter: Emitter<*>) {
        emitter.onError(ServerErrorException())
    }

    private fun onNotFoundRequest(emitter: Emitter<*>) {
        emitter.onError(Resources.NotFoundException())
    }
}



